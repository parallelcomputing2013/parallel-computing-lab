# @file Makefile
# @author Parallel Computing Group 22 (Manuel Geier, Thomas Rieder)
# @date 03.01.2014
#
# @brief make file parcomp project (prefix sums)
#
# use -DENDEBUG as a compiler flag to enable debugging
#
# when compiling cilk on saturn, use the following settings:
#    CILK_CC = /opt/cilk-5.4.6/bin/cilkc
#    CILK_CFLAGS = -O3 -pedantic -Wall -Wc,-std=c99 -cilk-profile -cilk-span -D_POSIX_C_SOURCE=200809L
# and set the following environment vars:
#    export LD_LIBRARY_PATH=/opt/cilk-5.4.6/lib/
#    export C_INCLUDE_PATH=/opt/cilk-5.4.6/include/
#

COMMON = src/common.c
DEBUG_FLAG = #-DENDEBUG

SEQ_CC = gcc
SEQ_CFLAGS = -O3 -std=c99 -Wall -pedantic -D_BSD_SOURCE -D_XOPEN_SOURCE=500
SEQ_LFLAGS = -lrt

#local:
CILK_CC = cilkc
CILK_CFLAGS = -O3 -pedantic -Wall -Wc,-std=c99 -cilk-profile -cilk-span -D_XOPEN_SOURCE=500 -D_BSD_SOURCE
CILK_LFLAGS = -lrt -lm

#on saturn:
#CILK_CC = /opt/cilk-5.4.6/bin/cilkc
#CILK_CFLAGS = -O3 -pedantic -Wall -Wc,-std=c99 -cilk-profile -cilk-span -D_POSIX_C_SOURCE=200809L
#CILK_LFLAGS = -lrt -lm

OPENMP_CC = gcc
OPENMP_CFLAGS = -fopenmp -O3 -pedantic -Wall -std=c99 -D_XOPEN_SOURCE=500 -D_BSD_SOURCE
OPENMP_LFLAGS = -lrt -lm

MPI_CC = mpicc
MPI_CFLAGS = -O3 -std=c99 -Wall -pedantic -D_BSD_SOURCE -D_XOPEN_SOURCE=500
MPI_LFLAGS = -lrt


all: init cilk openmp mpi seq

cilk: init bin/cilk_prefix_sum

openmp: init bin/openmp_iterative_prefix_sum bin/openmp_recursive_prefix_sum bin/openmp_blocked_hillis_steele_prefix_sum bin/openmp_hillis_steele_prefix_sum

mpi: init bin/mpi_prefix_sum

seq: init bin/seq_prefix_sum bin/sequential_prefix_sum


bin/cilk_prefix_sum: src/cilk_prefix_sum.cilk
	$(CILK_CC) $(CILK_CFLAGS) $(COMMON) $< -o $@ $(CILK_LFLAGS) $(DEBUG_FLAG)

bin/seq_prefix_sum: src/seq_prefix_sum.c
	$(SEQ_CC) $(SEQ_CFLAGS) $(COMMON) $< -o $@ $(SEQ_LFLAGS) $(DEBUG_FLAG)
 
bin/sequential_prefix_sum: src/sequential_prefix_sum.c
	$(SEQ_CC) $(SEQ_CFLAGS) $(COMMON) $< -o $@ $(SEQ_LFLAGS) $(DEBUG_FLAG)

bin/openmp_prefix_sum: src/openmp_prefix_sum.c
	$(OPENMP_CC) $(OPENMP_CFLAGS) $(COMMON) $< -o $@ $(OPENMP_LFLAGS) $(DEBUG_FLAG)

bin/openmp_iterative_prefix_sum: src/openmp_iterative_prefix_sum.c
	$(OPENMP_CC) $(OPENMP_CFLAGS) $(COMMON) $< -o $@ $(OPENMP_LFLAGS) $(DEBUG_FLAG)

bin/openmp_recursive_prefix_sum: src/openmp_recursive_prefix_sum.c
	$(OPENMP_CC) $(OPENMP_CFLAGS) $(COMMON) $< -o $@ $(OPENMP_LFLAGS) $(DEBUG_FLAG)

bin/openmp_hillis_steele_prefix_sum: src/openmp_hillis_steele_prefix_sum.c
	$(OPENMP_CC) $(OPENMP_CFLAGS) $(COMMON) $< -o $@ $(OPENMP_LFLAGS) $(DEBUG_FLAG)

bin/openmp_blocked_hillis_steele_prefix_sum: src/openmp_blocked_hillis_steele_prefix_sum.c
	$(OPENMP_CC) $(OPENMP_CFLAGS) $(COMMON) $< -o $@ $(OPENMP_LFLAGS) $(DEBUG_FLAG)
 
bin/mpi_prefix_sum: src/mpi_prefix_sum.c
	$(MPI_CC) $(MPI_CFLAGS) $(COMMON) $< -o $@ $(MPI_FLAGS)

init:
	test -d bin || mkdir bin

clean:
	rm -rf bin/*
	rm -rf doc/*
	rm -rf tmp/*

.PHONY: clean
