# Parallel Computing
----------------------------
Vienna University of Technology, Winter Term 2013

 * Manuel Geier
 * Thomas Rieder


## How to use Vagrant
```
vagrant box add "tu-parcomp" tu-parcomp.box
vagrant up
vagrant ssh
source /vagrant/.bash_profile 
```
