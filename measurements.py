#!/usr/bin/python
# extracts the data from *.log files and prints them csv format
# use it like: python measurements.py report/measurements/openmp/openmp_saturn-20140112-1936.log > report/measurements/openmp/openmp_saturn-20140112-1936.log.csv

import sys

if len(sys.argv) != 2:
    print 'Usage: python ' + str(sys.argv[0]) + ' <input-measurement-file-log>'
    exit()

arr_n = []
dict_threads = dict()
dict_seq = dict()
dict_cilk = dict()
dict_openmp = dict()
dict_mpi = dict()
current_n = -1

for line in open(str(sys.argv[1]),'r'):
    if "Input size:" in line:
        current_n = line.split(":")[1].strip()
        arr_n.append(current_n)
    if "threads" in line:
        dict_threads[''+current_n] = line.split(":")[1].strip()
    if "sequential time" in line:
        dict_seq[''+current_n] = line.split(":")[1].strip()
    if "openmp time" in line:
        dict_openmp[''+current_n] = line.split(":")[1].strip()
    if "cilk" in line:
        dict_cilk[''+current_n] = line.split(":")[1].strip()
    if "mpi" in line:
        dict_mpi[''+current_n] = line.split(":")[1].strip()

        
# print CSV

def print_csv_data(n, aDict):
    if ''+n in aDict.keys():
        sys.stdout.write(''+aDict[''+n])

sys.stdout.write('n;threads;seq;cilk;openmp;mpi\n')
for n in arr_n:
    sys.stdout.write(n)
    sys.stdout.write(';')
    print_csv_data(n, dict_threads)
    sys.stdout.write(';')
    print_csv_data(n, dict_seq)
    sys.stdout.write(';')
    print_csv_data(n, dict_cilk)
    sys.stdout.write(';')
    print_csv_data(n, dict_openmp)
    sys.stdout.write(';')
    print_csv_data(n, dict_mpi)
    sys.stdout.write('\n')
sys.stdout.flush()

