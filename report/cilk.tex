\subsection{Goal}
The algorithm has to work for all $n \in \mathbb N_0$. The solution has to be task parallel, work optimal and follow the divide-and-conquer paradigm. To avoid spawning too many parallel items, we use a sequential cut-off. Performance counters are used to verify the work-optimality of the algorithm.

\subsection{The Algorithm}

\begin{algorithm}
\caption{Cilk prefix-sum algorithm}
\label{alg:cilk1}
\begin{algorithmic} 
\REQUIRE $ array[0..n] $ with integers, $n>0$
\ENSURE prefix sums in $ array[0..n] $
\STATE $array \leftarrow$ generated test data
\STATE write\textunderscore to\textunderscore file($array$)
\IF{$n$ is a power of $2$}
\STATE $array[n-1] \leftarrow $ \textbf{spawn} $up(array, log2(n), n) $
\STATE \textbf{sync}
\STATE $array[n-1] \leftarrow 0$
\STATE \textbf{spawn} $down(array, 0, 0, n, 0)$
\STATE \textbf{sync}
\STATE fix the last element $array[n]$
\ELSE
\FOR{ each subset $s$ of $array$, such that $|s| = i$ is maximal and $i \in 2^x, x \in \mathbb N_0$ }
\STATE $s[2^i-1] \leftarrow$ \textbf{spawn} $up(s, log2(n), 2^i)$
\STATE \textbf{sync}
\ENDFOR
\FOR{ each subset $s$ of $array$, such that $|s| = i$ is maximal and $i \in 2^x, x \in \mathbb N_0$ }
\STATE $s[2^i-1] \leftarrow 0$
\STATE \textbf{spawn} $down(s, 0, 0, 2^i, remainder)$
\STATE \textbf{sync}
\STATE shift $s$ by $1$ to the \emph{left}
\STATE fix the last element $s[2^i-1]$
\STATE $remainder = s[2^i-1]$
\ENDFOR
\ENDIF
\STATE write\textunderscore to\textunderscore file($array$)
\end{algorithmic}
\end{algorithm}

The algorithm has been derived from the work of \emph{Blelloch}\cite{blelloch} and modified to use a divide-and-conquer approach (see Algorithm \ref{alg:cilk1}). As both the +-reduce (or "up-sweep") as well as the +-prescan (or "down-sweep") are operations on a binary tree, this gets a lot easier. Unfortunately, this whole approach only works for $n \in 2^x, x \in \mathbb N_0$. The details on how we modified our solution for arbitrary $n$ will be explained later. During development we also used the Cilk Reference\cite{cilkref} quite intensively.

\subsection{Up-Sweep}
The up-sweep starts at the last element of the array, \texttt{array[n]}. The value of this element is the result of the left "half" of the tree "$+$" the result of the right half. This is why there is a recursive call of $up(\cdot)$ with \texttt{array[n], n/2} and \texttt{array[n/2 - 1], n/2}. The result of the up-sweep is a binary tree where each node is the sum (or the result of the associative operation to be precise) of its two children.

\subsection{Down-Sweep}
Given the binary tree from the up-sweep, we set the root-element to 0 (the identity for the integer-sum). We then propagate this value to the left-most leaf node always switching the left with the right child node, while adding the value of the left one to the right one. The result is the prefix-sum of our input array with the first element being $0$ and the last element being excluded. This is why we have to fix the last element by shifting our result-array and setting \texttt{array[n]} to the previously saved value.

\subsection{Adjusting the algorithm for arbitrary n}
Since our algorithm is based on binary-trees, it only works for $n \in 2^x, x \in \mathbb N_0$. To fix this, we chose to partition our input array into blocks the size of powers of two. Our way of achieving this, was taking a look of the binary representation of $n$ and then carving out blocks the size of $2^d$, d being index of each bit which equals $1$. In order to keep our algorithm work-optimal we have to process the blocks in the decreasing order of size. Otherwise we would need one additional sequential run at the end to carry over the results of each block (which is similar to our MPI approach). To circumvent this problem we instead chose to carry over a remainder of each block in the down-phase itself. This remainder is then added to the result when reaching the bottom of the tree. This results in no additional associative operations and the algorithms stays work-optimal. 

\subsection{Work Optimality}
We have verified the work optimality using the performance counters as described in section~\ref{sec:optimality}. We expect the up- and down-sweep to do $n$-operations each, resulting a total of $2*n - 1$, n being the number of elements in the input array. Since the sequential algorithm $T_{seq}(n)$ is in $O(n)$, our algorithm's $W_{par}(n)$ is work optimal as it is in $O(n)$ as well, satisfying $W_{par}(n) = O(T_{seq}(n))$.
\\
\\
To illustrate this we have plotted our measured number of associative operations against the sequential algorithm as well as $f(x) = 2*x$ in figure \ref{fig:cilk_optimality}.

\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{evaluation/cilk/cilk_work_optimality.pdf}
\caption{Cilk Work Optimality}
\label{fig:cilk_optimality}
\end{figure}

\subsection{The Sequential Cutoff}
In order to avoid an oversaturation of Cilk threads, we have employed a sequential cut-off. After reaching a certain depth in the up- and down-trees, we will not \texttt{spawn} any additional threads, but solve the remaining tree sequentially. This actually leads to a significant improvement in performance. Especially with large input sets, spawning a new thread for each value is quite excessive.
\\
\\
\emph{Note: We initially simply wanted to use our recursive implementation but had performance issues. Closer investigation showed that the compiler didn't employ tail call optimization. For this reason we wrote the \texttt{for}-loops ourselves for an additional performance gain.}
\\
\\
In order to determine the optimal cut-off value we ran a number of tests on \texttt{saturn}. We tested cut-off depths between $1$ and $20$ with a number of different input-sizes (more on these in the next section) and 25 runs each. The cut-off with the minimal accumulated runtime was then chosen for all subsequent benchmarks. As we can see in figure \ref{fig:cilk_cutoff}, 15 seems to be an optimal value for \texttt{saturn's} 48 cores.

\begin{figure}[H]
\centering
\includegraphics[scale=0.65]{evaluation/cilk/cilk_seq_cutoff_plot.pdf}
\caption{Cilk Sequential Cut-off}
\label{fig:cilk_cutoff}
\end{figure}

\subsection{Benchmark Setup}
For all Cilk benchmarks we used the datatypes \texttt{long} or \texttt{int} with the following input values:
\\
\\
\texttt{2 4 8 10 15 16 63 64 1000 1023 1024 5000 10000 16384 60000 65535 65536}\\
\texttt{100000 500000 750000 1000000 2000000 4194303 4194304 5000000 8388608}\\
\texttt{10000000 16777215 16777216 20000000 30000000 33554432 67108864 134217728}
\\
\\
All elements $n, n \in 2^x$ are the best-case scenarios. We do not have to partition our input array and achieve optimal performance. Likewise all elements $n, n \in 2^x -1$ are the worst-case scenarios (as these number have a binary representation of \texttt{1}s only). These are the elements where we have to partition the input the most often. We also threw in some more or less arbitrary numbers like \texttt{5000} for good measure.
\\
\\
Using these values we tested both our sequential implementation as well as the Cilk algorithm with the following test cases (utilizing 25 test-runs):
\begin{itemize}
	\item Sequential Cut-off $s \in \{1, .., 20\}$
	\item Work Optimality
	\item Correctness
	\item Number of cores $c \in \{1, 2, 4, 8, 16, 32, 48\}$
\end{itemize}

\subsection{Benchmark Result}
Figure \ref{fig:cilk_cores} shows the overall performance of our implementation. Note that the x-axis is actually $\sqrt{n}$ and not $n$ in order to make the diagram more compact - which also causes seemingly polynomial growth rate. The jumps in the Cilk performance are the worst-case scenarios. As expected the sequential algorithm is quite hard to beat. It takes 16 cores to match its performance. Only with 32 and 48 cores are we actually able to surpass it. 
\\
\\
We are fully aware that our final test-case is a power of two and therefore the best-case for our algorithm. We would have liked to run additional test-cases with an even bigger $n$, but chose, out of consideration for other students, to not do so. Given the data however, we are quite confident to say that even tough these test-cases would cause a significant performance-hit, with a big enough $n$ our Cilk implementation should still be faster. A proper test case in our opinion would be an $n$, such that the sequential algorithm takes around 60 seconds.
\\
\\
For the final test case $n = 134217728$ we have the following speedup:
\\
\\
\begin{tabular}{ l l l l }
  \textbf{Number of Cores} & \textbf{Cilk time} & \textbf{Sequential} & \textbf{Speedup} ($T_{seq}/T_{par})$ \\
  16 Cores & 1.6284 & 1.6107 & 0.9891 \\
  32 Cores & 0.8565 & 1.6107 & 1.8805 \\
  48 Cores & 0.6674 & 1.6107 & 2.4131 \\
\end{tabular}
\\

\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{evaluation/cilk/cilk_core_count.pdf}
\caption{Cilk Overall Performance}
\label{fig:cilk_cores}
\end{figure}