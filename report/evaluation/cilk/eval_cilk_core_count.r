library(ggplot2)
library(reshape2)

# we don't want an exponential number display
options("scipen" = 10)

sequential <- read.csv("eval_cilk_saturn-seq.csv", header = TRUE, sep = ";", dec = ".")
cilk_1_core <- read.csv("eval_cilk_saturn-cores-1.csv", header = TRUE, sep = ";", dec = ".")
cilk_2_core <- read.csv("eval_cilk_saturn-cores-2.csv", header = TRUE, sep = ";", dec = ".")
cilk_4_core <- read.csv("eval_cilk_saturn-cores-4.csv", header = TRUE, sep = ";", dec = ".")
cilk_8_core <- read.csv("eval_cilk_saturn-cores-8.csv", header = TRUE, sep = ";", dec = ".")
cilk_16_core <- read.csv("eval_cilk_saturn-cores-16.csv", header = TRUE, sep = ";", dec = ".")
cilk_32_core <- read.csv("eval_cilk_saturn-cores-32.csv", header = TRUE, sep = ";", dec = ".")
cilk_48_core <- read.csv("eval_cilk_saturn-cores-48.csv", header = TRUE, sep = ";", dec = ".")


cilk_data <- data.frame(
    n=sequential[1], 
    seq=rowMeans(sequential[-1]),
    cilk1=rowMeans(cilk_1_core[-1]),
    cilk2=rowMeans(cilk_2_core[-1]),
    cilk4=rowMeans(cilk_4_core[-1]),
    cilk8=rowMeans(cilk_8_core[-1]),
    cilk16=rowMeans(cilk_16_core[-1]),
    cilk32=rowMeans(cilk_32_core[-1]),
    cilk48=rowMeans(cilk_48_core[-1]))

colnames(cilk_data) <- c("n", "Sequential", "Cilk 1 Core", "Cilk 2 Cores", "Cilk 4 Cores", "Cilk 8 Cores", "Cilk 16 Cores", "Cilk 32 Cores", "Cilk 48 Cores")

cilk_grouped <- melt(cilk_data, id=c("n"), variable="Algorithm")

pdf(file="cilk_core_count.pdf", width=10, height=7)

ggplot(data=cilk_grouped, aes(x=n, y=value, group=Algorithm, color=Algorithm)) +
    geom_point(size=3, shape=19, fill="white") +
    geom_line() +
    scale_x_sqrt() +
    xlab("sqrt(n)") +
    ylab("Average time") +
    #ggtitle("Cilk Overall Performance") +
    ylim(0,13)

dev.off()
