library(ggplot2)
library(reshape2)

# we don't want an exponential number display
options("scipen" = 10)

cutoff_data <- read.csv("eval_cilk_saturn-cutoff.csv", header = TRUE, sep = ",", dec = ".")

cutoff_result <- data.frame(n=seq(1:20))
cutoff_result$cutoff <- rowSums(t(cutoff_data[3:22]))

colnames(cutoff_result) <- c("n", "Sequential Cutoff")

cutoff_grouped <- melt(cutoff_result, id.vars="n", value.name="value", variable="Saturn")

pdf(file="cilk_seq_cutoff_plot.pdf")

ggplot(data=cutoff_grouped, aes(x=n, y=value, color=Saturn)) +
    geom_line() +
    geom_point(size=4, shape=19, fill="white") +
    xlab("Sequential Cutoff Depth") +
    ylab("Total Runtime for all test cases") +
    #ggtitle("Cilk Sequential Cutoff Performance for 48 cores") +
    theme(legend.position="none")

dev.off()