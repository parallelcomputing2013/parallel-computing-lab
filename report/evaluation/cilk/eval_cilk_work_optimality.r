library(ggplot2)
library(reshape2)

# we don't want an exponential number display
options("scipen" = 10)

work <- read.csv("eval_cilk_work_optimality.csv", header = TRUE, sep = ",", dec = ".")
work_grouped <- melt(work, id.vars="input", value.name="value", variable.name="n")

pdf(file="cilk_work_optimality.pdf", width=10, height=7)

ggplot(data=work_grouped, aes(x=input, y=value, group=n, color="Actual number of operations")) +
    geom_point(size=4, shape=19, fill="white") +
    stat_function(fun=function(x)x, geom="line", aes(color="O(n)")) +
    stat_function(fun=function(x)2*x, geom="line", aes(color="O(n) with a constant factor of 2")) +
    scale_color_manual("", values = c("black", "red", "dodgerblue2"), breaks=c("Actual number of operations", "O(n)", "O(n) with a constant factor of 2")) +
    xlab("Number of operations") +
    ylab("Input Size")
    #ggtitle("Cilk Work Optimality")   

dev.off()