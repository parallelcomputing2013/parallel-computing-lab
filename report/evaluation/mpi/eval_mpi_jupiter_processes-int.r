library(ggplot2)
library(reshape2)
library(matrixStats)

# we don't want an exponential number display
options("scipen" = 10)

sequential_int <- read.csv("eval_mpi_jupiter-seq-final-int.csv", header = FALSE, sep = ",", dec = ".")
mpi_1_proc_int <- read.csv("eval_mpi_jupiter-processes-1-int.csv", header = FALSE, sep = ",", dec = ".")
mpi_2_proc_int <- read.csv("eval_mpi_jupiter-processes-2-int.csv", header = FALSE, sep = ",", dec = ".")
mpi_4_proc_int <- read.csv("eval_mpi_jupiter-processes-4-int.csv", header = FALSE, sep = ",", dec = ".")
mpi_8_proc_int <- read.csv("eval_mpi_jupiter-processes-8-int.csv", header = FALSE, sep = ",", dec = ".")
mpi_16_proc_int <- read.csv("eval_mpi_jupiter-processes-16-int.csv", header = FALSE, sep = ",", dec = ".")

mpi_data_int <- data.frame(
    n=sequential_int[1][seq(1,28),], 
    seq=rowMeans(sequential_int[-1][seq(1,28),]),
    mpi1=rowMeans(mpi_1_proc_int[-1][seq(8,35),]),
    mpi2=rowMeans(mpi_2_proc_int[-1][seq(8,35),]),
    mpi4=rowMeans(mpi_4_proc_int[-1][seq(8,35),]),
    mpi8=rowMeans(mpi_8_proc_int[-1][seq(8,35),]),
    mpi16=rowMeans(mpi_16_proc_int[-1][seq(8,35),]))

colnames(mpi_data_int) <- c("n", "Sequential int", "MPI 1 Process int", "MPI 2 Processes int", "MPI 4 Processes int", "MPI 8 Processes int", "MPI 16 Processes int")

mpi_grouped_int <- melt(mpi_data_int, id=c("n"), variable="Processes")

pdf(file="mpi_process_count_int.pdf", width=10, height=7)

ggplot(data=mpi_grouped_int, aes(x=n, y=value, group=Processes, color=Processes)) +
    geom_point(size=3, shape=19, fill="white") +
    geom_line() +
    xlab("n") +
    ylab("Average time")
    #xlim(0, 33554433)
    #ggtitle("MPI Process Count Performance - int") +
    ylim(0,0.51)

dev.off()
