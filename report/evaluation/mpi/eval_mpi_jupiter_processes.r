library(ggplot2)
library(reshape2)

# we don't want an exponential number display
options("scipen" = 10)

sequential <- read.csv("eval_mpi_jupiter-seq-old.csv", header = TRUE, sep = ";", dec = ".")
mpi_1_proc <- read.csv("eval_mpi_jupiter-processes-1.csv", header = TRUE, sep = ";", dec = ".")
mpi_2_proc <- read.csv("eval_mpi_jupiter-processes-2.csv", header = TRUE, sep = ";", dec = ".")
mpi_4_proc <- read.csv("eval_mpi_jupiter-processes-4.csv", header = TRUE, sep = ";", dec = ".")
mpi_8_proc <- read.csv("eval_mpi_jupiter-processes-8.csv", header = TRUE, sep = ";", dec = ".")
mpi_16_proc <- read.csv("eval_mpi_jupiter-processes-16.csv", header = TRUE, sep = ";", dec = ".")

mpi_data <- data.frame(
    n=sequential[1], 
    seq=rowMeans(sequential[-1]),
    mpi1=rowMeans(mpi_1_proc),
    mpi2=rowMeans(mpi_2_proc),
    mpi4=rowMeans(mpi_4_proc),
    mpi8=rowMeans(mpi_8_proc),
    mpi16=rowMeans(mpi_16_proc))

colnames(mpi_data) <- c("n", "Sequential long", "MPI 1 Process long", "MPI 2 Processes long", "MPI 4 Processes long", "MPI 8 Processes long", "MPI 16 Processes long")

mpi_grouped <- melt(mpi_data, id=c("n"), variable="Processes")

pdf(file="mpi_process_count_long.pdf", width=10, height=7)

ggplot(data=mpi_grouped, aes(x=n, y=value, group=Processes, color=Processes)) +
    geom_point(size=3, shape=19, fill="white") +
    geom_line() +
    xlab("n") +
    ylab("Average time")
    #ggtitle("MPI Process Count Performance - long") +
    #ylim(0,0.4)

dev.off()
