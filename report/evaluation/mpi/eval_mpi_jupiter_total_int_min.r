library(ggplot2)
library(reshape2)

# we don't want an exponential number display
options("scipen" = 10)

sequential_host_int <- read.csv("eval_mpi_jupiter-seq-final-int.csv", header = FALSE, sep = ",", dec = ".")
mpi_1_host_int<- read.csv("eval_mpi_jupiter-1-host-int.csv", header = FALSE, sep = ";", dec = ".")
mpi_2_host_int <- read.csv("eval_mpi_jupiter-2-host-int.csv", header = FALSE, sep = ";", dec = ".")
mpi_4_host_int <- read.csv("eval_mpi_jupiter-4-host-int.csv", header = FALSE, sep = ";", dec = ".")
mpi_16_host_int <- read.csv("eval_mpi_jupiter-16-host-int.csv", header = FALSE, sep = ";", dec = ".")
mpi_34_host_int <- read.csv("eval_mpi_jupiter-34-host-int.csv", header = FALSE, sep = ";", dec = ".")


mpi_host_int <- data.frame(
    n=sequential_host_int[1], 
    seq=apply(sequential_host_int[-1], 1, min),
    mpi1=apply(mpi_1_host_int[-1][seq(6,35),], 1, min),
    mpi2=apply(mpi_2_host_int[-1][seq(6,35),], 1, min),
    mpi16=apply(mpi_16_host_int[-1][seq(6,35),], 1, min),
    mpi34=apply(mpi_34_host_int[-1][seq(6,35),], 1, min))

colnames(mpi_host_int) <- c("n", "Sequential", "MPI 1 Host", "MPI 2 Hosts", "MPI 16 Hosts", "MPI 34 Hosts")

mpi_grouped_int <- melt(mpi_host_int, id=c("n"), variable="Hosts")

pdf(file="mpi_final_int_minimum.pdf", width=10, height=7)

ggplot(data=mpi_grouped_int, aes(x=n, y=value, group=Hosts, color=Hosts)) +
    geom_point(size=3, shape=19, fill="white") +
    geom_line() +
    xlab("n") +
    ylab("Minimum time")
    #ggtitle("MPI Overall Performance with MPI Sync")

dev.off()

pdf(file="mpi_final_int_prediction_minimum.pdf", width=10, height=7)

ggplot(data=mpi_grouped_int, aes(x=n, y=value, group=Hosts, color=Hosts)) +
    xlab("n") +
    ylab("Minimum time") +
    #ggtitle("MPI Int Predicted Performance") +
    geom_smooth(method="lm", se=FALSE, fullrange=TRUE) +
    xlim(1,3*10^10)

dev.off()
