library(ggplot2)
library(reshape2)

# we don't want an exponential number display
options("scipen" = 10)

sequential_sync <- read.csv("eval_mpi_jupiter-seq-final-int.csv", header = TRUE, sep = ",", dec = ".")
mpi_1_host_sync <- read.csv("eval_mpi_jupiter-mpisync-1.csv", header = TRUE, sep = ",", dec = ".")
mpi_2_host_sync <- read.csv("eval_mpi_jupiter-mpisync-2.csv", header = TRUE, sep = ",", dec = ".")
mpi_4_host_sync <- read.csv("eval_mpi_jupiter-mpisync-4.csv", header = TRUE, sep = ",", dec = ".")
mpi_16_host_sync <- read.csv("eval_mpi_jupiter-mpisync-16.csv", header = TRUE, sep = ",", dec = ".")
#mpi_36_host_sync <- read.csv("eval_mpi_jupiter-36-host.csv", header = TRUE, sep = ";", dec = ".")


mpi_host_data_sync <- data.frame(
    n=sequential_sync[1], 
    seq=rowMeans(sequential_sync[-1]),
    mpi2=rowMeans(mpi_1_host_sync[-1]),
    mpi4=rowMeans(mpi_2_host_sync[-1]),
    mpi8=rowMeans(mpi_4_host_sync[-1]),
    mpi16=rowMeans(mpi_16_host_sync[-1]))

colnames(mpi_host_data_sync) <- c("n", "Sequential", "MPI 1 Host", "MPI 2 Hosts", "MPI 4 Hosts", "MPI 16 Hosts")

mpi_grouped_sync <- melt(mpi_host_data_sync, id=c("n"), variable="Hosts")

pdf(file="mpi_final_mpisync.pdf", width=10, height=7)

ggplot(data=mpi_grouped_sync, aes(x=n, y=value, group=Hosts, color=Hosts)) +
    geom_point(size=3, shape=19, fill="white") +
    geom_line() +
    #scale_x_sqrt() +
    xlab("n") +
    ylab("Average time") #+
  #  ggtitle("MPI Overall Performance with MPI Sync")

dev.off()

pdf(file="mpi_final_mpisync_prediction.pdf", width=10, height=7)

ggplot(data=mpi_grouped_sync, aes(x=n, y=value, group=Hosts, color=Hosts)) +
    xlab("n") +
    ylab("Average time") +
    #ggtitle("MPI Overall Predicted Performance") +
    geom_smooth(method="lm", se=TRUE, fullrange=TRUE) +
    xlim(1,3*10^10)

dev.off()
