library(ggplot2)
library(reshape2)

# we don't want an exponential number display
options("scipen" = 10)

sequential <- read.csv("eval_mpi_jupiter-seq-old.csv", header = TRUE, sep = ";", dec = ".")
mpi_1_host <- read.csv("eval_mpi_jupiter-1-host.csv", header = TRUE, sep = ";", dec = ".")
mpi_2_host <- read.csv("eval_mpi_jupiter-2-host.csv", header = TRUE, sep = ";", dec = ".")
mpi_4_host <- read.csv("eval_mpi_jupiter-4-host.csv", header = TRUE, sep = ";", dec = ".")
mpi_16_host <- read.csv("eval_mpi_jupiter-16-host.csv", header = TRUE, sep = ";", dec = ".")
mpi_36_host <- read.csv("eval_mpi_jupiter-36-host.csv", header = TRUE, sep = ";", dec = ".")


mpi_host_data <- data.frame(
    n=sequential[1], 
    seq=rowMeans(sequential[-1]),
    mpi2=rowMeans(mpi_1_host[-1]),
    mpi4=rowMeans(mpi_2_host[-1]),
    mpi8=rowMeans(mpi_4_host[-1]),
    mpi16=rowMeans(mpi_16_host[-1]),
    mpi32=rowMeans(mpi_36_host[-1]))

colnames(mpi_host_data) <- c("n", "Sequential", "MPI 1 Host", "MPI 2 Hosts", "MPI 4 Hosts", "MPI 16 Hosts", "MPI 36 Hosts")

mpi_grouped <- melt(mpi_host_data, id=c("n"), variable="Hosts")

pdf(file="mpi_host_count.pdf", width=10, height=7)

ggplot(data=mpi_grouped, aes(x=n, y=value, group=Hosts, color=Hosts)) +
    geom_point(size=3, shape=19, fill="white") +
    geom_line() +
    #scale_x_sqrt() +
    xlab("sqrt(n)") +
    ylab("Average time")# +
   # ggtitle("MPI Overall Performance with Send/Recv")

dev.off()

pdf(file="mpi_host_count_prediction.pdf", width=10, height=7)

ggplot(data=mpi_grouped, aes(x=n, y=value, group=Hosts, color=Hosts)) +
    xlab("n") +
    ylab("Average time") +
   # ggtitle("MPI Overall Predicted Performance") +
    geom_smooth(method="lm", se=FALSE, fullrange=TRUE) +
    xlim(1,3*10^10)

dev.off()
