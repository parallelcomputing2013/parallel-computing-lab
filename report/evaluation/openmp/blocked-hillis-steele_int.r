library(ggplot2)
library(reshape2)

# we don't want an exponential number display
options("scipen" = 10)

sequential    <- read.csv("data/20140202-0627_sequential_int.csv", header = FALSE, sep = ",", dec = ".")
openmp_bhs_1  <- read.csv("data/20140202-0627_openmp-blocked-hillis-steele_cores-1_int.csv", header = FALSE, sep = ",", dec = ".")
openmp_bhs_16 <- read.csv("data/20140202-0627_openmp-blocked-hillis-steele_cores-16_int.csv", header = FALSE, sep = ",", dec = ".")
openmp_bhs_32 <- read.csv("data/20140202-0627_openmp-blocked-hillis-steele_cores-32_int.csv", header = FALSE, sep = ",", dec = ".")
openmp_bhs_48 <- read.csv("data/20140202-0627_openmp-blocked-hillis-steele_cores-48_int.csv", header = FALSE, sep = ",", dec = ".")

openmp_data <- data.frame(
  n=sequential[1], 
  seq   =min(sequential[-1], na.rm=TRUE),
  bhs_1 =min(openmp_bhs_1[-1], na.rm=TRUE),
  bhs_16=min(openmp_bhs_16[-1], na.rm=TRUE),
  bhs_32=min(openmp_bhs_32[-1], na.rm=TRUE),
  bhs_48=min(openmp_bhs_48[-1], na.rm=TRUE))

colnames(openmp_data) <- c("n", "Sequential", "1 Cores", "16 Cores", "32 Cores", "48 Cores")

openmp_grouped <- melt(openmp_data, id=c("n"), variable="Algorithm")

pdf(file="openmp_blocked-hillis-steele_int_mins.pdf", width=10, height=7)

ggplot(data=openmp_grouped, aes(x=n, y=value, group=Algorithm, color=Algorithm)) +
  geom_point(size=2, shape=19, fill="white") +
  geom_line() +
  xlab("n") +
  ylab("Average time") +
  ggtitle("OpenMP Blocked Hillis-Steele Performance (int)")

dev.off()