library(ggplot2)
library(reshape2)

# we don't want an exponential number display
options("scipen" = 10)

seq   <- read.csv("data/20140201-1706_sequential_double.csv", header = FALSE, sep = ",", dec = ".")
omp.bhs <- read.csv("data/20140201-1706_openmp-blocked-hillis-steele_cores-48_double.csv", header = FALSE, sep = ",", dec = ".")

openmp_data <- data.frame(
  n=seq[1], 
  seq=rowMeans(seq[-1], na.rm=TRUE),
  bhs=rowMeans(omp.bhs[-1], na.rm=TRUE))

colnames(openmp_data) <- c("n", "Sequential", "Blocked Hillis-Steele")

openmp_grouped <- melt(openmp_data, id=c("n"), variable="Algorithm")

pdf(file="openmp_comparison_bhs-seq_double.pdf", width=10, height=7)

ggplot(data=openmp_grouped, aes(x=n, y=value, group=Algorithm, color=Algorithm)) +
  geom_point(size=2, shape=19, fill="white") +
  geom_line() +
  xlab("n") +
  ylab("Average time") +
  ggtitle("OpenMP Performance (48 cores) (double)")

dev.off()
