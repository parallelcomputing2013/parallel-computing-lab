library(ggplot2)
library(reshape2)

# we don't want an exponential number display
options("scipen" = 10)

seq   <- read.csv("data/20140202-1508_sequential_int.csv", header = FALSE, sep = ",", dec = ".")
omp.it   <- read.csv("data/20140202-1508_openmp-iterative_cores-8_int.csv", header = FALSE, sep = ",", dec = ".")
omp.re   <- read.csv("data/20140202-1508_openmp-recursive_cores-8_int.csv", header = FALSE, sep = ",", dec = ".")
omp.hs  <- read.csv("data/20140202-1508_openmp-hillis-steele_cores-8_int.csv", header = FALSE, sep = ",", dec = ".")
omp.bhs <- read.csv("data/20140202-1658_openmp-blocked-hillis-steele_cores-8_int.csv", header = FALSE, sep = ",", dec = ".")

openmp_data <- data.frame(
  n=seq[1], 
  seq=rowMeans(seq[-1], na.rm=TRUE),
  i=rowMeans(omp.it[-1], na.rm=TRUE),
  r=rowMeans(omp.re[-1], na.rm=TRUE),
  hs=rowMeans(omp.hs[-1], na.rm=TRUE),
  bhs=rowMeans(omp.bhs[-1], na.rm=TRUE))

colnames(openmp_data) <- c("n", "Sequential", "Iterative", "Recursive", "Hillis-Steele", "Blocked Hillis-Steele")

openmp_grouped <- melt(openmp_data, id=c("n"), variable="Algorithm")

pdf(file="openmp_comparison_int.pdf", width=10, height=7)

ggplot(data=openmp_grouped, aes(x=n, y=value, group=Algorithm, color=Algorithm)) +
  geom_point(size=2, shape=19, fill="white") +
  geom_line() +
  xlab("n") +
  ylab("Average time") +
  ggtitle("OpenMP Performance (8 cores) (int)")

dev.off()
