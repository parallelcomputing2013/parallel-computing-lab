library(ggplot2)
library(reshape2)

# we don't want an exponential number display
options("scipen" = 10)

openmp_sequential <- read.csv("openmp_sequential.csv", header = FALSE, sep = ",", dec = ".")
openmp_hillis <- read.csv("openmp-hillis-steele.csv", header = FALSE, sep = ",", dec = ".")
openmp_iter <- read.csv("openmp-iterative.csv", header = FALSE, sep = ",", dec = ".")

openmp_data <- data.frame(
  n=openmp_sequential[1], 
  seq=rowMeans(openmp_sequential[-1]),
  hillis=rowMeans(openmp_hillis[-1]),
  iter=rowMeans(openmp_iter[-1]))

colnames(openmp_data) <- c("n", "Sequential", "Hillis Steele", "Iterative")

openmp_grouped <- melt(openmp_data, id=c("n"), variable="Algorithm")

pdf(file="openmp_total.pdf", width=10, height=7)

ggplot(data=openmp_grouped, aes(x=n, y=value, group=Algorithm, color=Algorithm)) +
  geom_point(size=3, shape=19, fill="white") +
  geom_line() +
  xlab("n") +
  ylab("Average time") +
  ggtitle("OpenMP Overall Performance")

dev.off()

pdf(file="openmp_total_prediction.pdf", width=10, height=7)

ggplot(data=openmp_grouped, aes(x=n, y=value, group=Algorithm, color=Algorithm)) +
    geom_point(size=3, shape=19, fill="white") +
    xlab("n") +
    ylab("Average time") +
    ggtitle("OpenMP Overall Predicted Performance") +
    geom_smooth(method="lm", fullrange = TRUE) + 
    xlim(0, 10^9)

dev.off()
