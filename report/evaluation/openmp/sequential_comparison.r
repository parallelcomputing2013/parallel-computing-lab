library(ggplot2)
library(reshape2)

# we don't want an exponential number display
options("scipen" = 10)

sequential_i <- read.csv("data/20140202-0627_sequential_int.csv", header = FALSE, sep = ",", dec = ".")
sequential_l <- read.csv("data/sequential_long.csv", header = FALSE, sep = ",", dec = ".")
sequential_d <- read.csv("data/20140201-1706_sequential_double.csv", header = FALSE, sep = ",", dec = ".")

openmp_data <- data.frame(
  n=sequential_i[1], 
  a=rowMeans(sequential_i[-1], na.rm=TRUE),
  b=rowMeans(sequential_l[-1], na.rm=TRUE),
  c=rowMeans(sequential_d[-1], na.rm=TRUE))

colnames(openmp_data) <- c("n", "int", "long", "double")

openmp_grouped <- melt(openmp_data, id=c("n"), variable="Algorithm")

pdf(file="sequential_comparison.pdf", width=10, height=7)

ggplot(data=openmp_grouped, aes(x=n, y=value, group=Algorithm, color=Algorithm)) +
  geom_point(size=2, shape=19, fill="white") +
  geom_line() +
  xlab("n") +
  ylab("Average time") +
  ggtitle("Sequential comparison")

dev.off()