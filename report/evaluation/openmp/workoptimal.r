library(ggplot2)
library(reshape2)

# we don't want an exponential number display
options("scipen" = 10)

seq    <- read.csv("data/workoptimal/sequential.csv", header = FALSE, sep = ",", dec = ".")
iter <- read.csv("data/workoptimal/openmp-iterative.csv", header = FALSE, sep = ",", dec = ".")
rek <- read.csv("data/workoptimal/openmp-recursive.csv", header = FALSE, sep = ",", dec = ".")

openmp_data <- data.frame(
  n=seq[1], 
  seq=rowMeans(seq[-1], na.rm=TRUE),
  ite=rowMeans(iter[-1], na.rm=TRUE),
  rek=rowMeans(rek[-1], na.rm=TRUE))

colnames(openmp_data) <- c("n", "Sequential", "Iterative", "Recursive")

openmp_grouped <- melt(openmp_data, id=c("n"), variable="Algorithm")

pdf(file="workoptimal.pdf", width=10, height=7)

ggplot(data=openmp_grouped, aes(x=n, y=value, group=Algorithm, color=Algorithm)) +
  geom_point(size=2, shape=19, fill="white") +
  geom_line() +
  xlab("n") +
  ylab("Operations") +
  ggtitle("Work-Optimal")

dev.off()