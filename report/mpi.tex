\subsection{Goal}
Given a number of input values in an array of length $n$ we want to distribute said array to multiple nodes in a cluster. We will then compute the prefix sum of each block, communicate the remainder of these blocks between all nodes and end up with our distributed result array. For verification purposes we will then gather this array at a master process. This differs from the "conventional" approach of using \texttt{MPI\textunderscore Scan(...)} in that we distribute whole blocks instead of single elements.

\subsection{The Algorithm}

\begin{algorithm}[H]
\caption{MPI prefix-sum algorithm}
\label{alg:mpi1}
\begin{algorithmic} 
\REQUIRE $ array[0..n] $ with numbers, $n>0$
\ENSURE prefix sums in $ array[0..n] $
\STATE initialize \textbf{MPI}
\IF{$rank$ is 0}
	\STATE $array \leftarrow$ generated test data
	\STATE write\textunderscore to\textunderscore file($array$)
	\FOR{ each node $i$ }
		\STATE \textbf{MPI\textunderscore Send} blocksize
	\ENDFOR
	\STATE \textbf{MPI\textunderscore Scatterv}($array$)
	\STATE prefix\textunderscore sum($array$, blocksize)
	\STATE \textbf{MPI\textunderscore Exscan}($array$[blocksize-1])
	\STATE \textbf{MPI\textunderscore Gatherv}($array$)
	\STATE write\textunderscore to\textunderscore file($array$)
\ELSE
	\STATE \textbf{MPI\textunderscore Recv} array-size
	\STATE \textbf{MPI\textunderscore Scatterv}($array$)
	\STATE prefix\textunderscore sum($array$, blocksize)
	\STATE \textbf{MPI\textunderscore Exscan}($array$[blocksize-1])
	\FOR{ each $x \in array$ }
		\STATE $x \leftarrow x + offset $
	\ENDFOR
	\STATE \textbf{MPI\textunderscore Gatherv}($array$)
\ENDIF
\end{algorithmic}
\end{algorithm}

We have defined process \emph{0} to be the coordinator. This process generates the test data, calculates the size of the block each other process is going to get and distributes the data. Once every process has its data, we start measuring the performance. Each process calculates the prefix sum of its block in a sequential way and communicates the final element with the other processes using \texttt{MPI\textunderscore Exscan}. Once each process has the offset (= the final element of the previous block), they add this element to all array elements. At this point the prefix sum-calculation is finished, we stop the performance measurement and use \texttt{MPI\textunderscore Reduce} to determine the slowest process. In order to be able to verify the result with our sequential algorithm, we  gather the individual blocks and write them to a file. The pseudo-code can be examined in algorithm \ref{alg:mpi1}.
\\
\\
Our main references when implementing this algorithm were \cite{mpi1}, \cite{mpi2} and \cite{mpi3}.


\subsection{Benchmark Setup}
For the MPI benchmarks we used the datatype \texttt{long} as well as \texttt{int} with the following input values:
\\
\\
\texttt{2 4 8 10 15 16 63 64 1000 1023 1024 5000 10000 16384 30000 60000 65535 65536}\\
\texttt{100000 500000 750000 1000000 2000000 4194303 4194304 5000000 8388608}\\
\texttt{10000000 16777215 16777216 20000000 30000000 33554432 67108864 134217728}
\\
\\
Since the worst case in terms of data distribution is one process having $p-1$, $p$ being the number of processes in total across all hosts, elements less then the rest in its cluster, there are no "weak" and "strong" input values. Especially in our case where $p$ is $575$ at the most ($16$ cores $*$ $36$ hosts $-1$). One thing we expect to see when increasing the number of hosts is an increase in overhead. 36 hosts should require quite a bit more time to finish the final \texttt{MPI\textunderscore Exscan}, due to the communication taking place over an InfiniBand network. Additionally, due to most students using \texttt{jupiter0} for all their processes instead of distributing the work to the other nodes in the cluster and \texttt{jupiter19} being unavailable due to hardware failure, we have decided to exclude those hosts from our benchmark and go for a maxmium of $34$.
\\
\\
In total we ran the following test-cases (25 runs):
\begin{itemize}
  \item Correctness
  \item Number of hosts $h$ with 16 processes each, $h \in \{1, 2, 4, 16, 34 \}$ with \texttt{long}
  \item Number of hosts $h$ with 16 processes each, $h \in \{1, 2, 4, 16, 34 \}$ with \texttt{int}
  \item Number of processes $p$ on a single host, $p \in \{1, 2, 4, 8, 16 \}$  with \texttt{long}
  \item Number of processes $p$ on a single host, $p \in \{1, 2, 4, 8, 16 \}$ with \texttt{int}
\end{itemize}


\subsection{Benchmark Result}
First and foremost we tested our program on a single host with a different number of processes. If we hadn't been able to get a good speedup here, more hosts are certainly not going to help. As most students used \texttt{jupiter0} we started our benchmarks on \texttt{jupiter3}. We also wanted to see whether there was any big difference (meaning beyond the factor of 2 that we would expect) between the \texttt{long} and \texttt{int} datatype.
\\
\\
While the \texttt{long}-tests were fine [\ref{fig:mpi_long_success}], the results of the \texttt{int}-test turned out to be quite inconsistent [\ref{fig:mpi_int_fail}]. We saw that were a few other students running prefix sum with huge input values on the MPI cluster causing significant load.

\begin{figure}[H]
\centering
\includegraphics[scale=0.58]{evaluation/mpi/mpi_process_count_long.pdf}
\caption{Consistent MPI Process Count with Long}
\label{fig:mpi_long_success}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.58]{evaluation/mpi/mpi_process_count_int.pdf}
\caption{Inconsitent MPI Process Count with Integers}
\label{fig:mpi_int_fail}
\end{figure}

Unfortunately we did all the \texttt{long}-tests first and now had no way to properly test \texttt{int}-performance on \texttt{jupiter} in order to compare them to the \texttt{long}-results. Increasing the number of runs to $50$ and hoping to average out the noise also proved futile. One thing that was successful however, was using the minimum instead of the average of all of our runs - see figure \ref{fig:mpi_int_min} and \ref{fig:mpi_process_int_min}. Unfortunately there are only two runs in total where we achieved these results (we've probably caught a break in terms of server load).

\begin{figure}[H]
\centering
\includegraphics[scale=0.5]{evaluation/mpi/mpi_final_int_minimum.pdf}
\caption{MPI Host Count with Integer (minimum)}
\label{fig:mpi_int_min}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.5]{evaluation/mpi/mpi_process_count_int_last_minimum.pdf}
\caption{MPI Process Count with Integer (minimum)}
\label{fig:mpi_process_int_min}
\end{figure}

Using \texttt{long} we were however able to at least approximate the sequential performance (see figure \ref{fig:mpi_long_hosts}). As this was a benchmark we ran in early January, our biggest $n$ was just $33554432$. Given the server-load in late-January however, we were unable to properly test the bigger input values or even reach the same performance for $n = 33554432$. For this reason we have attempted to predict the progression of the function for larger $n$, as can be seen in figure \ref{fig:mpi_long_prediction}. The predictions were made by fitting linear models to given benchmark data using \texttt{R}\footnote{more information can be found here: \url{http://www.stats.ox.ac.uk/~konis/Rcourse/qr.pdf}}.

\begin{figure}[H]
\centering
\includegraphics[scale=0.58]{evaluation/mpi/mpi_host_count.pdf}
\caption{MPI Host Count with Long}
\label{fig:mpi_long_hosts}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.58]{evaluation/mpi/mpi_final_int_prediction_minimum.pdf}
\caption{MPI Host Count Prediction}
\label{fig:mpi_long_prediction}
\end{figure}

We think that our initial input values were too small to properly test the performance for $576$ processes. Given the prediction and the result of the integer-minimum benchmark \ref{fig:mpi_process_int_min} however, we conclude that for all $n > 10^9$ there \emph{should} be a significant performance gain. Here are the final results for the numerous numbers of hosts and $n = 536870912 $:
\\
\\
\begin{tabular}{ l l l l }
  \textbf{Number of Cores} & \textbf{MPI time} & \textbf{Sequential} & \textbf{Speedup} ($T_{seq}/T_{par})$ \\
  1 Host and 2 Processes & 0.35402 & 1.4645 & 4.1367\\
  1 Host and 4 Processes & 0.34119 & 1.4645 & 4.2923\\
  1 Host and 8 Processes & 0.38305 & 1.4645 & 3.8232\\
  1 Host and 16 Processes & 0.38024 & 1.4645 & 3.8515\\
  2 Hosts and 32 Processes & 0.52274 & 1.4645 & 2.8015\\
  16 Hosts and 256 Processes & 1.82381 & 1.4645 & 0.8029\\
  34 Hosts and 544 Processes & 0.89332 & 1.4645 & 1.6393\\
\end{tabular}




