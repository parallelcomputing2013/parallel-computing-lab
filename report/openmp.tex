
With OpenMP we had to implements four different algorithms:
\begin{enumerate}
\item Iterative (work-optimal)
\item Recursive (work-optimal)
\item Hillis-Steele
\item Blocked Hillis-Steele
\end{enumerate}

In the following sections we will describe the different implementations and our results.

\subsection{Iterative}

The iterative algorithm was split into two phases within a binary tree: the up- and the down-phase.

\begin{itemize}
\item Up-Phase: For every internal node of the tree, compute the sum of all the leaves in its subtree in a bottom-up fashion.
\item Down-Phase: Do a topdown computation to generate all the prefix sums.
\end{itemize}

\subsubsection{Work-optimal}

The sequential algorithm needs $ O(n) $ operations. The parallel iterative algorithm has $ O(2 * n/log(n) * log (n)) = O(n) $. So the parallel iterative algorithm is work-optimal.

See Figure~\ref{fig:openmpitwork}.

\begin{figure}[H]
\centering
\includegraphics[scale=0.58]{evaluation/openmp/workoptimal_iterative.pdf}
\caption{OpenMP iterative work-optimal}
\label{fig:openmpitwork}
\end{figure}

\subsection{Recursive}
We took our implementation from Cilk and transformed it into OpenMP. So the algorithm works the same (see Cilk section for further details on this implementation). 

\subsubsection{Cilk to OpenMP transformation}

At first we removed all `spawn` directives from all of the methods and analysed the `spawn` and `sync` statements. Method calls with no real parallel computation, were just replaces by normal sequential method calls. In Cilk they were needed to call the Cilk-methods, but in OpenMP this is no longer necessary.

Cilk:

\begin{lstlisting}
...
// up
arr[n-1] = spawn up_par(arr, log2(n), 0, n);
sync;
...
// down
spawn down_par(arr, 0, 0, n, 0);
sync;
...
\end{lstlisting}

OpenMP:

\begin{lstlisting}
...
// up
arr[n-1] = up_par(arr, log2(n), 0, n);
...
// down
down_par(arr, 0, 0, n, 0);
...
\end{lstlisting}

More complex statement, where parallism really takes place, were replaced by OpenMP directives with 'sections'.

Cilk:

\begin{lstlisting}
...
spawn down_par(arr, d+1, offset, n, prev_remainder);
spawn down_par(arr, d+1, offset + inc, n, prev_remainder);
sync;
...
\end{lstlisting}
		
OpenMP:

\begin{lstlisting}
#pragma omp parallel
{
	#pragma omp sections
	{
		#pragma omp section
		{
			down_par(arr, d+1, offset, n, prev_remainder);
		}
		#pragma omp section
		{
			down_par(arr, d+1, offset + inc, n, prev_remainder);
		}
	}
} // implied barrier at the end of parallel directive
\end{lstlisting}

\subsubsection{Work-optimal}

The sequential algorithm needs $ O(n) $ operations. The parallel recursive algorithm needs also $ O(n) $ operations (same like in section for work-optimality for Cilk). See Figure \ref{fig:openMpRecursiveWorkOptimal}.

\begin{figure}[H]
\centering
\includegraphics[scale=0.55]{evaluation/openmp/workoptimal_recursive.pdf}
\caption{OpenMP recursive work-optimal}
\label{fig:openMpRecursiveWorkOptimal}
\end{figure}

		
\subsection{Hillis-Steele}

The Hillis-Steele was done with a double-buffered version of the prefix sum, which is based on some pseudo-code from \emph{GPU Gems 3}\cite{gpugems3} . The base concept is, that there are multiple iterations and in every iteration data elements can steal from previous calculated values from other data elements.

\begin{lstlisting}
// A Double-Buffered Version of the Sum Scan
generic_type* hillis_steele_prefix(generic_type* arr, generic_type* arrHelp, long n) {
	for(int d=1; d <= log2(n)+1; d++) {
		#pragma omp parallel for
		for(int k=0; k<n; k++) {
			if(k >= pow(2,d-1)) {
				(arrHelp)[k] = (arr)[(int)(k - pow(2, d-1))] + (arr)[k];
			} else {
				(arrHelp)[k] = (arr)[k];
			}
		}
		
		// swap arrays, so that in arr are the right values
		generic_type* arrTmp = arr;
		arr = arrHelp; 
		arrHelp = arrTmp;
	}
	return arr;
}
\end{lstlisting}


\subsection{Blocked Hillis-Steele}

The Blocked Hillis-Steele consists of three phases:
\begin{enumerate}
\item The data is splitted into data blocks and every thread gets one block, where it calculates the prefix sum for its block.
\item After that, the main thread takes the highest element from each block and creates the prefix sum from this.
\item In the last step, every thread takes (/steals) the calculated the sum of the previous blocks from the main thread and adds it to his own block.
\end{enumerate}

See Figure \ref{fig:openMpBhSIllustration} for illustration.

\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{illustrations/BlockedHillisSteele_illustration.pdf}
\caption{OpenMP Blocked Hillis-Steele illustration}
\label{fig:openMpBhSIllustration}
\end{figure}

Our implementation of the algorithm splits the data blocks the following way:
\\
\\
$ blockSize = \floor*{\dfrac{n}{numberOfThreads}} $

$ start_i = blockSize * threadNum_i $

$ end_i = blockSize * threadNum_i + blockSize - 1$
\\
\\
If there is a remainder for the division of the blocksize, the last thread will also get those elements to calculate. One improvement would be to evenly split the remaining elements to all thread, which are available. 
\\
\\
The calculation of a block looks like that:
\begin{lstlisting}
for(int i=start+1; i<=end; i++) {
	arr[i] = arr[i-1] + arr[i];
}
\end{lstlisting}

For the second step, the end values of each block are copied into a second array.
\begin{lstlisting}
arrHelp[threadNum] = arr[end];
\end{lstlisting}

The main thread can easily calculate prefix sum on that:
\begin{lstlisting}
for(int i=1; i<numberOfThreads; i++) {
	arrHelp[i] = arrHelp[i-1] + arrHelp[i];
}
\end{lstlisting}

The final step is to recalculate each block with the previous block sums:
\begin{lstlisting}
for(int i=start; i<=end; i++) {
	arr[i] = arr[i] + arrHelp[threadNum-1];
}
\end{lstlisting}

\subsection{Performance}
With type \textit{long} the parallel implementation was nearly the same as the sequential one, as you can see in Figure \ref{fig:openMpBhSPerformanceLong}. With type \textit{double} it was also nearly the same as you can see in Figure \ref{fig:openMpBhSPerformanceDouble}, but if you have a look at the last measurement, we thought that, if n was even higher we were able to beat the sequential algorithm significantly. But during the huge load on the test server, we were not able to prove that.

\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{evaluation/openmp/openmp_blocked-hillis-steele_double.pdf}
\caption{OpenMP Blocked Hillis-Steele (double)}
\label{fig:openMpBhSPerformanceDouble}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{evaluation/openmp/openmp_blocked-hillis-steele_long.pdf}
\caption{OpenMP Blocked Hillis-Steele (long)}
\label{fig:openMpBhSPerformanceLong}
\end{figure}


\subsection{Measurements}

Time measurement was done with \verb|omp_get_wtime()|, a method, provided by OpenMP, to measure the real time (not CPU time).

\begin{lstlisting}
// start benchmark
double t_start omp_get_wtime(), t_end;
t_start = omp_get_wtime();

// actual code ...

// stop benchmark
t_end = omp_get_wtime();
(void) fprintf(stdout, "Total openmp time: %.5f\n", t_end - t_start);
\end{lstlisting}

\subsection{OpenMP algorithm comparisons}

We made some measurements to compare different implementations of our prefix algorithms which were based on OpenMP. Figure \ref{fig:openMpComparisonLong} shows the comparison with datatype \textit{long}, Figure \ref{fig:openMpComparisonDouble} shows the comparison with data type \textit{double} and Figure \ref{fig:openMpComparisonInt} shows the comparison with data type \textit{int}. The Blocked Hillis-Steele overlaps with the sequential implementation in those plots.

We saw that only \textit{Blocked Hillis-Steele} had a chance to beat the sequential algorithm. With all other algorithms we would need a major improvement. See figure \ref{fig:openMpComparisonBhsSeqLong}, \ref{fig:openMpComparisonBhsSeqDouble} and \ref{fig:openMpComparisonBhsSeqInt}.

In the following table you can see the speedups of our different implemetnations. $Time$ is the time which was used to calculate $n = 134217728$ numbers. $Mean-Speedup$ is the average speedup for all our input-$n$s. See Bennchmark Setup and Result in the Cilk section for further details our benchmarking.
\\
\\
long: $T_{seq} = 1.5129060$

\begin{tabular}{ l l l l l }
  \textbf{Algorithmn} & \textbf{Cores} & \textbf{Time} & \textbf{Speedup} ($T_{seq}/T_{par})$ & \textbf{Mean-Speedup} \\
  Hillis-Steele & 36 & 37.0890792 & 0.040791145 & 0.03030225 \\
  Iterative & 36 & 56.926014582 & 0.026576707 & 0.0205153 \\
  Blocked Hillis-Steele & 16 & 1.543202 & 0.9803681 & 1.16009 \\
  Blocked Hillis-Steele & 32 & 1.585 & 0.9545148 & 1.130206 \\
  Blocked Hillis-Steele & 48 & 1.67243 & 0.9046154 & 1.272299 \\
  Recursive & 36 & 12.778152 & 0.118397872 & 0.03826745 \\
\end{tabular}
\\
\\
double: $T_{seq} = 0.4939832$

\begin{tabular}{ l l l l l }
  \textbf{Algorithmn} & \textbf{Cores} & \textbf{Time} & \textbf{Speedup} ($T_{seq}/T_{par})$ & \textbf{Mean-Speedup} \\
  Hillis-Steele & 36 & 61.4040136 & 0.00804480312 & 0.003402613 \\
  Iterative & 36 & 43.98082972 & 0.01123178447 & 0.004336242 \\
  Blocked Hillis-Steele & 16 & 0.55277157 & 0.8936479795 & 0.2332175 \\
  Blocked Hillis-Steele & 32 & 0.48527732 & 1.0179400096 & 0.2463943 \\
  Blocked Hillis-Steele & 48 & 0.45933703 & 1.0754264676 & 0.2432311 \\
  Recursive & 36 & 16.377458 & 0.0301623854 & 0.007642812 \\
\end{tabular}
\\
\\
int: $T_{seq} = 0.1486433$

\begin{tabular}{ l l l l l }
  \textbf{Algorithmn} & \textbf{Cores} & \textbf{Time} & \textbf{Speedup} ($T_{seq}/T_{par})$ & \textbf{Mean-Speedup} \\
  Hillis-Steele & 8 & 100.412215333 & 0.0014803312 & 0.001486427 \\
  Iterative & 8 & 6.0636116803 & 0.0245139928 & 0.01244788 \\
  Blocked Hillis-Steele & 8 & 0.2456026167 & 0.6052188505 & 0.1818377 \\
  Recursive & 8 & 5.204174667 & 0.02856232599 & 0.008155335 \\
\end{tabular}


\begin{figure}[H]
\centering
\includegraphics[scale=0.59]{evaluation/openmp/openmp_comparison_long.pdf}
\caption{OpenMP comparison (long)}
\label{fig:openMpComparisonLong}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.59]{evaluation/openmp/openmp_comparison_double.pdf}
\caption{OpenMP comparison (double)}
\label{fig:openMpComparisonDouble}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.59]{evaluation/openmp/openmp_comparison_int.pdf}
\caption{OpenMP comparison (int)}
\label{fig:openMpComparisonInt}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.59]{evaluation/openmp/openmp_comparison_bhs-seq_long.pdf}
\caption{OpenMP comparison Blocked Hillis-Steele vs. Sequential (long)}
\label{fig:openMpComparisonBhsSeqLong}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.59]{evaluation/openmp/openmp_comparison_bhs-seq_double.pdf}
\caption{OpenMP comparison Blocked Hillis-Steele vs. Sequential (double)}
\label{fig:openMpComparisonBhsSeqDouble}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.59]{evaluation/openmp/openmp_comparison_bhs-seq_int.pdf}
\caption{OpenMP comparison Blocked Hillis-Steele vs. Sequential (int)}
\label{fig:openMpComparisonBhsSeqInt}
\end{figure}