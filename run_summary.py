#!/usr/bin/python

import sys
from os import listdir
from os.path import isfile, join

if len(sys.argv) != 3:
    print 'Usage: python ' + str(sys.argv[0]) + ' <folder> <filematch>'
    exit()

path = sys.argv[1]
filematch = sys.argv[2]
    
files = [ f for f in listdir(path) if "_"+filematch+"_" in f and isfile(path+f) ]

arr_n = []
runs = []
current_n = -1
for file in files:
    run_dict = dict()
    runs.append(run_dict)
    for line in open(path+file,'r'):
        if "Input size:" in line:
            current_n = line.split(":")[1].strip()
            if not ''+current_n in arr_n:
                arr_n.append(''+current_n)
        if "time" in line:
            run_dict[''+current_n] = line.split(":")[1].strip()

outfile = open(path+filematch+".csv", 'w')
for n in arr_n:
    outfile.write(n+",")
    for run in runs:
        outfile.write(str(run.get(n))+",")
    outfile.write("\n")
outfile.close()
