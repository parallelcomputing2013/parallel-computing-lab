#!/usr/bin/env bash

#########################
# Environment
#########################
ENV_SERVER="server"
ENV_MG="mg"
ENV_TR="tr"
ENV_DEMO="demo"

# choose the environment
ENV=$ENV_DEMO


#########################
# Test config
#########################

TYPE="int"

sequential_benchmark=true

cilk_sequential_cutoff=false
cilk_normal_benchmark=true
cilk_work_optimality=false
cilk_cores_benchmark=false

openmp_iterative_benchmark=true
openmp_recursive_benchmark=true
openmp_hillis_steele_benchmark=true
openmp_blocked_hillis_steele_benchmark=true

mpi_normal_benchmark=true
mpi_nec_benchmark=false
mpi_openmpi_benchmark=false
mpi_mpich_benchmark=false
mpi_number_of_processes=false


#########################
# Configure test runs
#########################


if [ "$ENV" = "$ENV_SERVER" ] ; then
    NUMBER_OF_PROCESSORS=48
    NUMBER_OF_MPI_PROCCESSES=16
    NUMBER_OF_RUNS=25
    DO_CHECK=false
fi
if [ "$ENV" = "$ENV_MG" ] ; then
    NUMBER_OF_PROCESSORS=4
    NUMBER_OF_MPI_PROCCESSES=16
    NUMBER_OF_RUNS=15
    DO_CHECK=false
fi
if [ "$ENV" = "$ENV_DEMO" ] ; then
    NUMBER_OF_PROCESSORS=4
    NUMBER_OF_MPI_PROCCESSES=4
    NUMBER_OF_RUNS=1
    DO_CHECK=true
    echo "Warnining: Running in Demo Mode!"
fi

echo "Using limited number of input values!"
echo ""

input_values=( 2 4 8 10 15 16 63 64 1000 1023 1024 5000 10000 16384 30000 60000 65535 65536 100000 500000 750000 1000000 ) # 2000000 4194303 4194304 5000000 8388608 10000000 16777215 16777216 20000000 30000000 33554432 67108864 134217728 )
input_values_2pow=( 2 4 8 16 32 64 128 256 512 1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576 ) # 2097152 4194304 8388608 16777216 33554432 67108864 134217728 268435456 536870912 1073741824 )

cilk_values=( 2 4 8 10 15 16 63 64 1000 1023 1024 5000 10000 16384 30000 60000 65535 65536 100000 500000 750000 1000000 ) #2000000 4194303 4194304 5000000 8388608 10000000 16777215 16777216 20000000 30000000 33554432 67108864 134217728 )
cilk_number_cores=(1 2 4) # 8 16 32 48)

openmp_values_2pow=( 2 4 8 16 32 64 128 256 512 1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576) #2097152 4194304 8388608 16777216 33554432 67108864 134217728 268435456 536870912 1073741824 )
openmp_values=( 2 4 8 10 15 16 63 64 1000 1023 1024 5000 10000 16384 30000 60000 65535 65536 100000 500000 750000) #1000000 2000000 4194303 4194304 5000000 8388608 10000000 16777215 16777216 20000000 30000000 33554432 67108864 134217728 )
    openmp_number_cores=( 4 )

if [ "$ENV" = "$ENV_SERVER" ] ; then
    openmp_values=( 2 4 8 10 15 16 63 64 1000 1023 1024 5000 10000 16384 30000 60000 65535 65536 100000 500000 750000 1000000 2000000 4194303 4194304 5000000 8388608 10000000 16777215 16777216 20000000 30000000 33554432 67108864 134217728 )
    openmp_number_cores=( 1 16 32 48 )
fi
if [ "$ENV" = "$ENV_MG" ] ; then
    #input_values=( 2 4 8 10 15 16 63 64 1000 1023 1024 5000 10000 16384 30000 60000 65535 65536 100000 500000 750000 1000000 2000000 )
    #openmp_values=( 2 4 8 10 15 16 63 64 1000 1023 1024 5000 10000 16384 30000 60000 65535 65536 100000 500000 750000 1000000 2000000 )
    #openmp_number_cores=(1 2 4)

    input_values=( 2 4 8 10 15 16 63 64 1000 1023 1024 5000 10000 16384 30000 60000 65535 65536 100000 500000 750000 1000000 2000000 4194303 4194304 5000000 8388608 10000000 16777215 16777216 20000000 30000000 33554432 67108864 134217728 )
    openmp_values=( 2 4 8 10 15 16 63 64 1000 1023 1024 5000 10000 16384 30000 60000 65535 65536 100000 500000 750000 1000000 2000000 4194303 4194304 5000000 8388608 10000000 16777215 16777216 20000000 30000000 33554432 67108864 134217728 )
    openmp_number_cores=( 4 )
fi

mpi_values=( 2 4 8 10 15 16 63 64 1000 1023 1024 5000 10000 16384 30000 60000 65535 65536 100000 500000 750000 1000000) # 2000000 4194303 4194304 5000000 8388608 10000000 16777215 16777216 20000000 30000000 33554432 67108864 134217728)
mpi_number_processes=( 1 2 4 ) #8 16 )


#########################
# Prepare
#########################

TEST_TIMESTAMP=`date +%Y%m%d-%H%M`
LOGFOLDER="logs/${TEST_TIMESTAMP}/"
LOGPREFIX="${LOGFOLDER}${TEST_TIMESTAMP}"

test -d logs || mkdir logs
test -d $LOGFOLDER || mkdir $LOGFOLDER

echo -e "Parallel Computing Test Runner"
echo -e "------------------------------\n"

echo -e "=== Preparing Environment ==="
mkdir bin &> /dev/null
mkdir tmp &> /dev/null
make clean
make all

echo -e "type: ${TYPE}"

#######################################
# SEQUENTIAL
#######################################

echo -e ""
echo -e "=== Testing Sequential ==="

if $sequential_benchmark ; then
    numcores=1
    for runnr in $(seq 1 $NUMBER_OF_RUNS)
    do
        val_c=1
        val_len=${#input_values[@]}
        for i in "${input_values[@]}"
        do
            echo -e "seq, cores: $numcores, run: $runnr of $NUMBER_OF_RUNS, input: $val_c of $val_len"
            log="${LOGPREFIX}_sequential_run-${runnr}.log"
            echo -e "Input size: $i" &>> $log
            echo -e "type: ${TYPE}" &>> $log
            bin/sequential_prefix_sum -n $i &>> $log
            ((val_c++))
        done
    done
else
    echo "Skipping sequential tests"
fi


#######################################
# CILK
#######################################

echo -e ""
orte-clean

echo -e "=== Testing Cilk ==="

echo -e "Running tests...\n"

#
# to evaluate:
#    for i in `ls | grep cutoff`; do echo $i; cat $i | grep cilk | awk '{s+=$4} END {print s}'; echo -e ""; done
#

if $cilk_sequential_cutoff ; then
    echo -e "Running tests on the sequential cutoff"
    for i in $(seq 1 $NUMBER_OF_RUNS)
    do
        timestamp=`date +%Y%m%d-%H%M`
        PREV_I=`expr $i - 1`

        echo "Using Cutoff $i" >> cilk_saturn-cutoff-$timestamp.log

        sed -i "s/#define SEQ_CUTOFF ($PREV_I)/#define SEQ_CUTOFF ($i)/g" src/cilk_prefix_sum.cilk

        make clean
        make

        for i in "${cilk_values[@]}"
        do
            echo -e "Input size: $i" >> cilk_saturn-cutoff-$timestamp.log
            bin/cilk_prefix_sum -stats 1 -nproc $NUMBER_OF_PROCESSORS -n $i &>> cilk_saturn-cutoff-$timestamp.log
            bin/seq_prefix_sum -n $i &>> cilk_saturn-cutoff-$timestamp.log
        done
    done
else
    echo -e "Skipping tests on the sequential cutoff"
fi


if $cilk_normal_benchmark ; then
    echo -e "Running cilk benchmarks"
    echo -e "Using $NUMBER_OF_PROCESSORS cores"
    for i in $(seq 1 $NUMBER_OF_RUNS)
    do
        echo -e "Doing test run #$i"
        timestamp=`date +%Y%m%d-%H%M`
        for i in "${cilk_values[@]}"
        do
            echo -e "Input size: $i" &>> cilk_saturn-$timestamp.log
            bin/cilk_prefix_sum -stats 1 -nproc $NUMBER_OF_PROCESSORS -n $i &>> cilk_saturn-$timestamp.log
            bin/seq_prefix_sum -n $i &>> cilk_saturn-$timestamp.log
        done
    done
else
    echo "Skipping normal cilk benchmarks"
fi

if $cilk_work_optimality ; then
    echo "Running tests for work optimality"
    echo -e "Using $NUMBER_OF_PROCESSORS cores"

    sed -i "s/#define ENABLE_WORKCOUNT (false)/#define ENABLE_WORKCOUNT (true)/g" src/cilk_prefix_sum.cilk

    make clean
    make

    for n in ${cilk_number_cores[@]}
    do
        echo -e "Using $n core(s)"
        timestamp=`date +%Y%m%d-%H%M`
        for i in "${cilk_values[@]}"
        do
            echo -e "Input size: $i" &>> cilk_saturn-optimality-$n-$timestamp.log
            bin/cilk_prefix_sum -nproc $n -n $i | grep operations &>> cilk_saturn-optimality-$n-$timestamp.log
        done
    done

    sed -i "s/#define ENABLE_WORKCOUNT (true)/#define ENABLE_WORKCOUNT (false)/g" src/cilk_prefix_sum.cilk

else
    echo "Skipping tests for work optimality"
fi

if $cilk_cores_benchmark ; then
    echo -e "Running cilk number of cores benchmarks"

    for i in $(seq 1 $NUMBER_OF_RUNS)
    do
        echo -e "Doing test run #$i"
        for n in ${cilk_number_cores[@]}
        do
            echo -e "Using $n core(s)"
            timestamp=`date +%Y%m%d-%H%M`
            for i in "${cilk_values[@]}"
            do
                echo -e "Input size: $i" &>> cilk_saturn-cores-$n-$timestamp.log
                bin/cilk_prefix_sum -stats 1 -nproc $n -n $i &>> cilk_saturn-cores-$n-$timestamp.log
                bin/seq_prefix_sum -n $i &>> cilk_saturn-cores-$n-$timestamp.log
            done
        done
    done
else
    echo "Skipping number of cores benchmarks"
fi


#######################################
# OPENMP
#######################################

echo -e ""
echo -e "=== Testing OpenMP ==="

if $openmp_iterative_benchmark ; then
    numcores=$NUMBER_OF_PROCESSORS
    export OMP_NUM_THREADS=$numcores
    export OMP_DYNAMIC=FALSE
    for runnr in $(seq 1 $NUMBER_OF_RUNS)
    do
        val_c=1
        val_len=${#openmp_values[@]}
        for i in "${openmp_values[@]}"
        do
            echo -e "omp-iter, cores: $numcores, run: $runnr of $NUMBER_OF_RUNS, input: $val_c of $val_len"
            log="${LOGPREFIX}_openmp-iterative_cores-${numcores}_run-${runnr}.log"
            echo -e "Input size: $i" &>> $log
            echo -e "type: ${TYPE}" &>> $log
            bin/openmp_iterative_prefix_sum -n $i &>> $log
            if $DO_CHECK ; then
                bin/seq_prefix_sum -n $i &>> $log
            fi
            ((val_c++))
        done
    done
else
    echo "Skipping OpenMP iterative tests"
fi


if $openmp_recursive_benchmark ; then
    numcores=$NUMBER_OF_PROCESSORS
    export OMP_NUM_THREADS=$numcores
    export OMP_DYNAMIC=FALSE
    for runnr in $(seq 1 $NUMBER_OF_RUNS)
    do
        val_c=1
        val_len=${#openmp_values[@]}
        for i in "${openmp_values[@]}"
        do
            echo -e "omp-rec, cores: $numcores, run: $runnr of $NUMBER_OF_RUNS, input: $val_c of $val_len"
            log="${LOGPREFIX}_openmp-recursive_cores-${numcores}_run-${runnr}.log"
            echo -e "Input size: $i" &>> $log
            echo -e "type: ${TYPE}" &>> $log
            bin/openmp_recursive_prefix_sum -n $i &>> $log
            if $DO_CHECK ; then
                bin/seq_prefix_sum -n $i &>> $log
            fi
            ((val_c++))
        done
    done
else
    echo "Skipping OpenMP recursive tests"
fi


if $openmp_hillis_steele_benchmark ; then
    numcores=$NUMBER_OF_PROCESSORS
    export OMP_NUM_THREADS=$numcores
    export OMP_DYNAMIC=FALSE
    echo "Running OpenMP Hillis-Steele tests"
    for runnr in $(seq 1 $NUMBER_OF_RUNS)
    do
        c=1
        val_len=${#openmp_values[@]}
        for i in "${openmp_values[@]}"
        do
            echo -e "omp-hs, cores: $numcores, run: $runnr of $NUMBER_OF_RUNS, input: $c of $val_len"
            log="${LOGPREFIX}_openmp-hillis-steele_cores-${numcores}_run-${runnr}.log"
            echo -e "Input size: $i" &>> $log
            echo -e "type: ${TYPE}" &>> $log
            bin/openmp_hillis_steele_prefix_sum -n $i &>> $log
            if $DO_CHECK ; then
                bin/seq_prefix_sum -n $i &>> $log
            fi
            ((c++))
        done
    done
else
    echo "Skipping OpenMP Hillis-Steele tests"
fi


if $openmp_blocked_hillis_steele_benchmark ; then

    echo "Running OpenMP Blocked Hillis-Steele tests"
    export OMP_DYNAMIC=FALSE

    for numcores in "${openmp_number_cores[@]}"
    do
        export OMP_NUM_THREADS=$numcores

        for runnr in $(seq 1 $NUMBER_OF_RUNS)
        do
            val_c=1
            arrlen=${#openmp_values[@]}
            for i in "${openmp_values[@]}"
            do
                echo -e "omp-bhs, cores: $numcores, run: $runnr of $NUMBER_OF_RUNS, input: $val_c of $arrlen"
                log="${LOGPREFIX}_openmp-blocked-hillis-steele_cores-${numcores}_run-${runnr}.log"
                echo -e "Input size: $i" &>> $log
                echo -e "type: ${TYPE}" &>> $log
                bin/openmp_blocked_hillis_steele_prefix_sum -n $i &>> $log
                if $DO_CHECK ; then
                    bin/seq_prefix_sum -n $i &>> $log
                fi
                ((val_c++))
            done
        done

    done
else
    echo "Skipping OpenMP Blocked Hillis-Steele tests"
fi


#######################################
# MPI
#######################################

echo -e "=== Testing MPI ==="

mypath=$PATH

if $mpi_normal_benchmark ; then

    PATH=$mypath:/opt/NECmpi/gcc/inst/bin

    echo "Running basic tests for MPI performance"

    echo "Testing with 1 host (16 processes)"
    for j in $(seq 1 $NUMBER_OF_RUNS)
    do
        echo -e "Doing test run #$j"
        timestamp=`date +%Y%m%d-%H%M`

        for i in "${mpi_values[@]}"
        do
            echo -e "Input size: $i" &>> mpi_jupiter-1-$timestamp.log
            mpirun -np $NUMBER_OF_MPI_PROCCESSES bin/mpi_prefix_sum -n $i &>> mpi_jupiter-1-$timestamp.log
            bin/seq_prefix_sum -n $i &>> mpi_jupiter-1-$timestamp.log
        done
    done

    if [ "$ENV" != "$ENV_DEMO" ] ; then
        echo "Testing with 2 hosts (32 processes)"
        for j in $(seq 1 $NUMBER_OF_RUNS)
        do
            echo -e "Doing test run #$j"
            timestamp=`date +%Y%m%d-%H%M`

            for i in "${mpi_values[@]}"
            do
                echo -e "Input size: $i" &>> mpi_jupiter-2-$timestamp.log
                mpirun -host jupiter0 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter1 -np $NUMBER_OF_MPI_PROCCESSES bin/mpi_prefix_sum -n $i &>> mpi_jupiter-2-$timestamp.log
                bin/seq_prefix_sum -n $i &>> mpi_jupiter-2-$timestamp.log
            done
        done

        echo "Testing with 4 hosts (64 processes)"
        for j in $(seq 1 $NUMBER_OF_RUNS)
        do
            echo -e "Doing test run #$j"
            timestamp=`date +%Y%m%d-%H%M`

            for i in "${mpi_values[@]}"
            do
                echo -e "Input size: $i" &>> mpi_jupiter-4-$timestamp.log
                mpirun -host jupiter0 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter1 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter2 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter3 -np $NUMBER_OF_MPI_PROCCESSES  bin/mpi_prefix_sum -n $i &>> mpi_jupiter-4-$timestamp.log
                bin/seq_prefix_sum -n $i &>> mpi_jupiter-4-$timestamp.log
            done
        done

        echo "Testing with 16 hosts (256 processes)"
        for j in $(seq 1 $NUMBER_OF_RUNS)
        do
            echo -e "Doing test run #$j"
            timestamp=`date +%Y%m%d-%H%M`

            for i in "${mpi_values[@]}"
            do
                echo -e "Input size: $i" &>> mpi_jupiter-16-$timestamp.log
                mpirun -host jupiter0 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter1 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter2 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter3 -np $NUMBER_OF_MPI_PROCCESSES -host    jupiter4 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter5 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter6 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter7 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter8 -np $ NUMBER_OF_MPI_PROCCESSES -host jupiter9 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter10 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter11 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter12 -np $  NUMBER_OF_MPI_PROCCESSES -host jupiter13 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter14 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter15 -np $NUMBER_OF_MPI_PROCCESSES bin/mpi_prefix_sum -n $i &>>   mpi_jupiter-16-$timestamp.log
                bin/seq_prefix_sum -n $i &>> mpi_jupiter-16-$timestamp.log
            done
        done

        echo "Testing with 36 hosts (576 processes)"
        for j in $(seq 1 $NUMBER_OF_RUNS)
        do
            echo -e "Doing test run #$j"
            timestamp=`date +%Y%m%d-%H%M`

            for i in "${mpi_values[@]}"
            do
                echo -e "Input size: $i" &>> mpi_jupiter-36-$timestamp.log
                mpirun -host jupiter0 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter1 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter2 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter3 -np $NUMBER_OF_MPI_PROCCESSES -host    jupiter4 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter5 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter6 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter7 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter8 -np $ NUMBER_OF_MPI_PROCCESSES -host jupiter9 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter10 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter11 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter12 -np $  NUMBER_OF_MPI_PROCCESSES -host jupiter13 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter14 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter15 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter16 -np $  NUMBER_OF_MPI_PROCCESSES -host jupiter17 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter18 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter19 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter20 -np $  NUMBER_OF_MPI_PROCCESSES -host jupiter21 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter22 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter23 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter24 -np $  NUMBER_OF_MPI_PROCCESSES -host jupiter25 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter26 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter27 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter28 -np $  NUMBER_OF_MPI_PROCCESSES -host jupiter29 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter30 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter31 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter32 -np $  NUMBER_OF_MPI_PROCCESSES -host jupiter33 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter34 -np $NUMBER_OF_MPI_PROCCESSES -host jupiter35 -np $NUMBER_OF_MPI_PROCCESSES bin/mpi_prefix_sum -n $i &>>   mpi_jupiter-36-$timestamp.log
                bin/seq_prefix_sum -n $i &>> mpi_jupiter-36-$timestamp.log
            done
        done
    else
        echo "In Demo mode - skipping tests with multiple hosts"
    fi
else
    echo "Skipping basic MPI performance tests"
fi

if $mpi_mpich_benchmark ; then
    PATH=$mypath:/opt/mpich/bin

    orte-clean
    make clean
    make

    echo "Running mpich tests for MPI"
    for j in $(seq 1 $NUMBER_OF_RUNS)
    do
        echo -e "Doing test run #$j"
        timestamp=`date +%Y%m%d-%H%M`

        for i in "${mpi_values[@]}"
        do
            echo -e "Input size: $i" &>> mpi_jupiter-mpich-$timestamp.log
            mpirun -np $NUMBER_OF_MPI_PROCCESSES bin/mpi_prefix_sum -n $i &>> mpi_jupiter-mpich-$timestamp.log
            bin/seq_prefix_sum -n $i &>> mpi_jupiter-mpich-$timestamp.log
        done
    done
else
    echo "Skipping mpich performance tests"
fi



if $mpi_openmpi_benchmark ; then
    PATH=$mypath:/opt/openmpi/bin

    orte-clean
    make clean
    make

    echo "Running OpenMPI tests for MPI"
    for j in $(seq 1 $NUMBER_OF_RUNS)
    do
        echo -e "Doing test run #$j"
        timestamp=`date +%Y%m%d-%H%M`

        for i in "${mpi_values[@]}"
        do
            echo -e "Input size: $i" &>> mpi_jupiter-openmpi-$timestamp.log
            mpirun -np $NUMBER_OF_MPI_PROCCESSES bin/mpi_prefix_sum -n $i &>> mpi_jupiter-openmpi-$timestamp.log
            bin/seq_prefix_sum -n $i &>> mpi_jupiter-openmpi-$timestamp.log
        done
    done
else
    echo "Skipping OpenMPI performance tests"
fi


if $mpi_nec_benchmark ; then
    PATH=$mypath:/opt/NECmpi/gcc/inst/bin

    orte-clean
    make clean
    make

    echo "Running NEC tests for MPI"
    for j in $(seq 1 $NUMBER_OF_RUNS)
    do
        echo -e "Doing test run #$j"
        timestamp=`date +%Y%m%d-%H%M`

        for i in "${mpi_values[@]}"
        do
            echo -e "Input size: $i" &>> mpi_jupiter-nec-$timestamp.log
            mpirun -np $NUMBER_OF_MPI_PROCCESSES bin/mpi_prefix_sum -n $i &>> mpi_jupiter-nec-$timestamp.log
            bin/seq_prefix_sum -n $i &>> mpi_jupiter-nec-$timestamp.log
        done
    done
else
    echo "Skipping NEC MPI performance tests"
fi


if $mpi_number_of_processes ; then
    PATH=$mypath

    echo "Running MPI number of processes tests"
    for n in  "${mpi_number_processes[@]}"
    do
        echo -e "Number of processes: $n"

        for j in $(seq 1 $NUMBER_OF_RUNS)
        do
            echo -e "Doing test run #$j"
            timestamp=`date +%Y%m%d-%H%M`

            for i in "${mpi_values[@]}"
            do
                echo -e "Input size: $i" &>> mpi_jupiter-processes-$n-$timestamp.log
                mpirun -np $n bin/mpi_prefix_sum -n $i &>> mpi_jupiter-processes-$n-$timestamp.log
                bin/seq_prefix_sum -n $i &>> mpi_jupiter-processes-$n-$timestamp.log
            done
        done
    done
else
    echo "Skipping MPI number of processes tests"
fi
