#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <errno.h>
#include <math.h>
#include <stdarg.h>
#include <time.h>
#include <limits.h>
#include <assert.h>
#include "common.h"

void fill_with_random_numbers(generic_type* arr, long len)
{
    long i;
    for(i=0; i<len; i++)
    {
        arr[i] = (generic_type) rand_lim_int(RANDOM_MAX);
    }
}

void fill_with_ones(generic_type* arr, long len)
{
    long i;
    for(i=0; i<len; i++)
    {
        arr[i] = 1;
    }
}

void print_arr(generic_type* arr, long len)
{
    long i;
    for(i=0; i<len; i++)
    {
        DEBUG(TYPE_OUT, arr[i]);
        if(i != len-1) {
            DEBUG(", ");
        }
    }
    DEBUG("\n");
}


void bail_out(int eval, const char* fmt, ...)
{
    va_list ap;

    if (fmt != NULL) {
        va_start(ap, fmt);
        (void) vfprintf(stderr, fmt, ap);
        va_end(ap);
    }

    (void) fprintf(stderr, "\n");

    exit(eval);
}

int rand_lim_int(int limit)
{
    int divisor = RAND_MAX / (limit + 1);
    int retval;

    do {
        retval = rand() / divisor;
    } while (retval > limit);

    return retval;
}

void write_arr_to_file(generic_type* arr, long len, const char* file)
{
    FILE *f = fopen(file, "w");
    if(f == NULL) {
            (void) fprintf(stderr, "Error opening file!\n");
            bail_out(EXIT_FAILURE, "couldn't open file");
    }

    long i;
    for(i = 0; i < len; i++) {
        (void) fprintf(f, TYPE_OUT2, arr[i]);
    }

    fclose(f);
}

void read_arr_from_file(generic_type* arr, long len, const char* file)
{
    FILE *f = fopen(file, "r");
    if(f == NULL) {
            (void) fprintf(stderr, "Error opening file!\n");
            bail_out(EXIT_FAILURE, "couldn't open file");
    }

    generic_type temp = 0, ret = 1;
    long i = 0;

    if(fscanf(f, TYPE_OUT, &temp) > 0) {
        while(!feof (f) && i < len && ret > 0) {
            arr[i] = temp;
            i++;
            ret = fscanf(f, TYPE_OUT, &temp);
        }
    } else {
        (void) fclose(f);
    }
}

long parse_args(int argc, char* argv[]) {
    long n;
    char* program_name = argv[0];

    int opt;
    bool opt_n = false;
    while ( (opt = getopt(argc, argv, "n:")) != -1) {
        switch (opt) {
            case 'n':
                if(opt_n) {
                  usage(program_name);
                  bail_out(EXIT_FAILURE, "duplicate argument");
                } else {
                    if (sscanf (optarg, "%ld", &n) != 1) {
                        usage(program_name);
                        bail_out(EXIT_FAILURE, "invalid argument type");
                    }
                    opt_n = true;
                }
                break;
            case '?':
                usage(program_name);
                bail_out(EXIT_SUCCESS, "invalid argument");
                break;
            default:
                assert(0);
        }
    }

    if(opt_n == false) {
        usage(program_name);
        bail_out(EXIT_FAILURE, "missing argument");
    }

    if(argc > optind) {
        usage(program_name);
        bail_out(EXIT_FAILURE, "too many arguments");
    }

    return n;
}

void usage(char* program_name)
{
    (void) fprintf(stderr,"Usage: ./%s -n NUMBER_OF_ELEMENTS\n\n", program_name);
    (void) exit(EXIT_FAILURE);
}