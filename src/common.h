/**
 * @file common.h
 * @author Parallel Computing Group 22 (Manuel Geier, Thomas Rieder)
 * @date 03.01.2014
 *
 * @brief common functions for all our programs
 *
 **/

#ifndef COMMON_H
#define COMMON_H

/******* TYPE *******/
typedef int generic_type;

#define TRANSFER_TYPE MPI_INT
#define TYPE_OUT "%d"
#define TYPE_OUT2 "%d\n"

/******* CHECK FLAG *******/
/* if this is `true`, the generated number input and the result is written to a file */
#define DO_CHECK (false)


#ifdef ENDEBUG
#define DEBUG(...) do { fprintf(stderr, __VA_ARGS__); } while(0)
#else
#define DEBUG(...)
#endif

#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))

#define RANDOM_MAX (20)
#define FILE_INPUT_ARRAY ("tmp/prefix_input")
#define FILE_RESULT_ARRAY ("tmp/prefix_result")

enum boolean {false, true};
typedef enum boolean bool;

/**
 * @brief terminate program on program error
 * @param eval exit code
 * @param fmt format string
 */
void bail_out(int eval, const char* fmt, ...);

/**
 * @brief fills an array with random numbers
 * @param arr array that will be filled
 * @param len number of elements
 */
void fill_with_random_numbers(generic_type* arr, long len);

/**
 * @brief fills an array only with the number 1
 * @param arr array that will be filled
 * @param len number of elements
 */
void fill_with_ones(generic_type* arr, long len);

/**
 * @brief prints n elements from an array to stdout
 * @param arr source array
 * @param len number of elements
 */
void print_arr(generic_type* arr, long len);

/**
 * @brief return a random number between 0 and limit inclusive
 * @param limit upper limit of the random number
 * @return random number
 */
int rand_lim_int(int limit);

/**
 * @brief writes number from an array to a file, separated by '\n'
 * @param arr source array
 * @param len number of elements
 * @param name of the file
 */
void write_arr_to_file(generic_type* arr, long len, const char* file);

/**
 * @brief reads numbers from a file into an array; assumes malloc has been called
 * @param arr target array
 * @param len number of elements
 * @param name of the file
 */
void read_arr_from_file(generic_type* arr, long len, const char* file);

/**
 * @brief parses the integer argument for our programs
 * @param argc argument counter
 * @param argument vector
 * @return prefix sum array size
 */
long parse_args(int argc, char* argv[]);

/**
 * @brief print usage message
 */
void usage(char* program_name);

#endif
