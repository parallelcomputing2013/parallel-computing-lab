/**
 * MPI implementation of prefix-sums
 * by Manuel Geier, Thomas Rieder
 *
 * [1] = https://www.cac.cornell.edu/ranger/MPIcc/exercise.aspx
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <errno.h>
#include <math.h>
#include <stdarg.h>
#include <time.h>
#include <limits.h>
#include <assert.h>
#include <mpi.h>
#include "common.h"

/* rank of the coordinator */
#define COORDINATOR (0)

const char *progname = "mpi_prefix_sum";

/* timers for benchmark */
double start, delta;

void seq_prefix_sum(generic_type* arr, long len);

void distribute_work(long n, int world_size, generic_type* arr);

void worker_prefix(long n, int world_size, int rank);


int main(int argc, char* argv[]) {

    /* number of elements */
    long n = parse_args(argc, argv);

    /* 1 is the maximum number of shifts we might do to correct for the identity element */
    generic_type* arr = (generic_type*) malloc(sizeof(generic_type) * (n));

    int rank, size;

    /* start MPI */
    MPI_Init (&argc, &argv);
    MPI_Comm_rank (MPI_COMM_WORLD, &rank);
    MPI_Comm_size (MPI_COMM_WORLD, &size);


    if(rank == 0) {
        //DEBUG("i am the coordinator\n");
        //DEBUG("the MPI size is %d\n", size);

        if(n <= 0) {
            (void) fprintf(stdout, "n = 0, nothing to do");
            bail_out(EXIT_SUCCESS, "n = 0, nothing to do");
        }

        /* seed the random number generator */
        DEBUG("seeding srand...\n");
        srand(time(NULL));

        /* init test data */
        fill_with_random_numbers(arr, n);
        DEBUG("printing the input array...\n");
        print_arr(arr, n);

        write_arr_to_file(arr, n, FILE_INPUT_ARRAY);

        distribute_work(n, size, arr);

        double min;
        MPI_Reduce(&delta, &min, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);

        (void) fprintf(stdout, "Total mpi time: %.5f\n", min);

        /* output */
        DEBUG("printing the result array...\n");
        print_arr(arr, n);

        write_arr_to_file(arr, n, FILE_RESULT_ARRAY);

    } else {
        worker_prefix(n, size, rank);
        MPI_Reduce(&delta, NULL, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
    }

    MPI_Finalize();

    return 0;
}



void distribute_work(long n, int world_size, generic_type* arr) {

    if(world_size > 1) {
        DEBUG("attempting to distirubte the work on %ld elements to %d nodes\n", n, world_size);

        int nmin = n / world_size;
        int nextra = n % world_size;

        int sendcounts[world_size - 1];
        int displs[world_size - 1];

        int e;

        // taken from [1]

        int k, i;
        for(i = 0, k = 0; i < world_size; i++) {
            if( i < nextra) {
                sendcounts[i] = nmin + 1;
            } else {
                sendcounts[i] = nmin;
            }

            displs[i] = k;
            k = k + sendcounts[i];

            //DEBUG("sendcounts: %d; displs: %d\n", sendcounts[i], displs[i]);
        }

        e = sendcounts[0];

        generic_type* coord_arr = (generic_type*) malloc(sizeof(generic_type) * e);

        // tell each process their block size
        for(i = 1; i < world_size; i++) {
            MPI_Send(&sendcounts[i], 1, MPI_INT, i, 0, MPI_COMM_WORLD);
        }

        // scatter the input array
        MPI_Scatterv(arr, sendcounts, displs, TRANSFER_TYPE, coord_arr, e, TRANSFER_TYPE, 0, MPI_COMM_WORLD);

        /* start measurements */
        start = MPI_Wtime();

        print_arr(coord_arr, e);
        seq_prefix_sum(coord_arr, e);

        generic_type offset;
        MPI_Exscan(&coord_arr[e-1], &offset, 1, TRANSFER_TYPE, MPI_SUM, MPI_COMM_WORLD);

        /* stop measurements */
        delta = MPI_Wtime() - start;

        MPI_Gatherv(coord_arr, e, TRANSFER_TYPE, arr, sendcounts, displs, TRANSFER_TYPE, 0, MPI_COMM_WORLD);
    } else {
        start = MPI_Wtime();
        seq_prefix_sum(arr, n);
        delta = MPI_Wtime() - start;
    }
}

void worker_prefix(long n, int world_size, int rank) {

    MPI_Status status;

    int e, i;

    /* get number of elements */
    MPI_Recv(&e, 1, MPI_INT, COORDINATOR, 0, MPI_COMM_WORLD, &status);

    generic_type* arr = (generic_type*) malloc(sizeof(generic_type) * e);

    MPI_Scatterv(NULL, NULL, NULL, TRANSFER_TYPE, arr, e, TRANSFER_TYPE, 0, MPI_COMM_WORLD);

    /* start measurements */
    start = MPI_Wtime();

    seq_prefix_sum(arr, e);

    generic_type offset;
    MPI_Exscan(&arr[e-1], &offset, 1, TRANSFER_TYPE, MPI_SUM, MPI_COMM_WORLD);

    for(i = 0; i < e; i++) {
        arr[i] += offset;
    }

    delta = MPI_Wtime() - start;

    MPI_Gatherv(arr, e, TRANSFER_TYPE, NULL, 0, 0, TRANSFER_TYPE, 0, MPI_COMM_WORLD);
}

void seq_prefix_sum(generic_type* arr, long len)
{
    if(len > 0)
    {
        generic_type sum = arr[0];
        for(long i=1; i<=len-1; i++)
        {
            sum += arr[i];
            arr[i] = sum;
        }
    }
}