/**
Blocked Hillis Steele prefix sum algorithm with OpenMP
by Manuel Geier, Thomas Rieder

Usage:
./openmp_blocked_hillis_steele_prefix_sum -n NUMBER_OF_ELEMENTS
**/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <errno.h>
#include <math.h>
#include <stdarg.h>
#include <time.h>
#include <limits.h>
#include <string.h>
#include "common.h"
#include <omp.h>

/* enables work count measurements */
#define ENABLE_WORKCOUNT (false)
//#define WORKCOUNTER() (increment_workcount()) //on
#define WORKCOUNTER() //off

/* Name of the program */
const char *progname = "openmp_prefix_sum";

/* work count */
long count = 0;

void increment_workcount() {
	#pragma omp critical
	{
	    count++;
	}
}

generic_type* blocked_hillis_steele_prefix(generic_type* arr, generic_type* arrHelp, long n) {

	// den array durch die anzahl der threads teilen und berechnen lassen
	#pragma omp parallel
	{
		int numberOfThreads = omp_get_num_threads();
		int blockSize = -1;
		if(n < numberOfThreads) {
			numberOfThreads = n;
		}
		blockSize = n / numberOfThreads;
		
		int threadNum = omp_get_thread_num();
		int start = -1, end = -1;

		if(threadNum < numberOfThreads) {

			// calculated blocks
			if(threadNum == numberOfThreads - 1) {
				start = threadNum * blockSize;
				end = n - 1;
			} else {
				start = threadNum * blockSize;
				end = start + blockSize - 1;
			}
		
			// calculate block 
			for(int i=start+1; i<=end; i++) {
				arr[i] = arr[i-1] + arr[i];							WORKCOUNTER();
			}

			// save last value
			arrHelp[threadNum] = arr[end];							WORKCOUNTER();
		}
		#pragma omp barrier

			if(threadNum == 0) {
				// prefix sum
				for(int i=1; i<numberOfThreads; i++) {
					arrHelp[i] = arrHelp[i-1] + arrHelp[i];			WORKCOUNTER();
				}
			}

		#pragma omp barrier
		
		if(threadNum < numberOfThreads) {
			if(threadNum > 0) {
				// add to block 
				for(int i=start; i<=end; i++) {
					arr[i] = arr[i] + arrHelp[threadNum-1];			WORKCOUNTER();
				}
			}
		}
	}
	
	return arr;
}


int main(int argc, char* argv[]) {

	/* number of elements */
	long n = parse_args(argc, argv);

	/* �nput/output array and helper array */
	generic_type* arr = (generic_type*) malloc(sizeof(generic_type) * n);
	generic_type* arrHelp = (generic_type*) malloc(sizeof(generic_type) * n);

	if(n <= 0) {
		(void) fprintf(stdout, "n = 0, nothing to do\n\n");
		bail_out(EXIT_SUCCESS, "n = 0, nothing to do");
	}

	/* seed the random number generator */
	DEBUG("seeding srand...\n");
	srand(time(NULL));

	/* init test data */
	fill_with_random_numbers(arr, n);
	DEBUG("printing the input array...\n");
	print_arr(arr, n);

    if(DO_CHECK) {
        write_arr_to_file(arr, n, FILE_INPUT_ARRAY);
    }

	#pragma omp parallel
	{
		if(omp_get_thread_num() == 0) {
			int numberOfThreads = omp_get_num_threads();
			printf("threads: %d\n", numberOfThreads);
		}
	}

	// start measurement
	double t_start, t_end;
	t_start = omp_get_wtime();

    // Calculate
	arr = blocked_hillis_steele_prefix(arr, arrHelp, n);

    // measurement
    t_end = omp_get_wtime();
	(void) fprintf(stdout, "Total openmp time: %.10f\n", t_end - t_start);

	/* output */
	DEBUG("printing the result array...\n");
	print_arr(arr, n);

    if(DO_CHECK) {
        write_arr_to_file(arr, n, FILE_RESULT_ARRAY);
    }

	if(ENABLE_WORKCOUNT)
		(void) fprintf(stdout, "The total number of operations is: %ld\n", count);

  	return 0;
}
