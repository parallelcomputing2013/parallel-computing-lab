/**
Hillis steele prefix sum algorithm with OpenMP
by Manuel Geier, Thomas Rieder

Usage:
./openmp_hillis_steele_prefix_sum -n NUMBER_OF_ELEMENTS
**/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <errno.h>
#include <math.h>
#include <stdarg.h>
#include <time.h>
#include <limits.h>
#include <string.h>
#include "common.h"
#include <omp.h>

/*
 * sequential cutoff (level/current depth of the tree)
 *
 *
 */
#define SEQ_CUTOFF (15)

/* enables work count measurements */
#define ENABLE_WORKCOUNT (true)
//#define WORKCOUNTER() (increment_workcount()) //on
#define WORKCOUNTER() //off

/* Name of the program */
const char *progname = "openmp_prefix_sum";

/* work count */
long count = 0;

void increment_workcount() {
	#pragma omp critical
	{
	    count++;
	}
}

// A Double-Buffered Version of the Sum Scan
generic_type* hillis_steele_prefix(generic_type* arr, generic_type* arrHelp, long n) {

	for(int d=1; d <= log2(n)+1; d++) {
		#pragma omp parallel for
		for(int k=0; k<n; k++) {
			if(k >= pow(2,d-1)) {
				(arrHelp)[k] = (arr)[(int)(k - pow(2, d-1))] + (arr)[k];         WORKCOUNTER();
			} else {
				(arrHelp)[k] = (arr)[k];
			}
		}
		
		// swap arrays, so that in arr are the right values
		generic_type* arrTmp = arr;
		arr = arrHelp; 
		arrHelp = arrTmp;
	}
	
	return arr;
}


int main(int argc, char* argv[]) {

	/* number of elements */
	long n = parse_args(argc, argv);

	/* �nput/output array and helper array */
	generic_type* arr = (generic_type*) malloc(sizeof(generic_type) * n);
	generic_type* arrHelp = (generic_type*) malloc(sizeof(generic_type) * n);

	if(n <= 0) {
		(void) fprintf(stdout, "n = 0, nothing to do\n\n");
		bail_out(EXIT_SUCCESS, "n = 0, nothing to do");
	}

	/* seed the random number generator */
	DEBUG("seeding srand...\n");
	srand(time(NULL));

	/* init test data */
	fill_with_random_numbers(arr, n);
	DEBUG("printing the input array...\n");
	print_arr(arr, n);

    if(DO_CHECK) {
        write_arr_to_file(arr, n, FILE_INPUT_ARRAY);
    }

	// start measurement
	double t_start, t_end;
	t_start = omp_get_wtime();

    // Calculate
	arr = hillis_steele_prefix(arr, arrHelp, n);

    // measurement
    t_end = omp_get_wtime();
	(void) fprintf(stdout, "Total openmp time: %.5f\n", t_end - t_start);

	/* output */
	DEBUG("printing the result array...\n");
	print_arr(arr, n);

    if(DO_CHECK) {
        write_arr_to_file(arr, n, FILE_RESULT_ARRAY);
    }

	if(ENABLE_WORKCOUNT)
		(void) fprintf(stdout, "The total number of operations is: %ld\n", count);

  	return 0;
}
