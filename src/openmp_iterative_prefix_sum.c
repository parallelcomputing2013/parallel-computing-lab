/**
Iterative prefix sum algorithm with OpenMP
by Manuel Geier, Thomas Rieder

Usage:
./openmp_iteratrive_prefix_sum -n NUMBER_OF_ELEMENTS
**/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <errno.h>
#include <math.h>
#include <stdarg.h>
#include <time.h>
#include <limits.h>
#include <string.h>
#include "common.h"
#include <omp.h>

/* enables work count measurements */
#define ENABLE_WORKCOUNT (false)
//#define WORKCOUNTER() (increment_workcount()) //on
#define WORKCOUNTER() //off

/* Name of the program */
const char *progname = "openmp_iterative_prefix_sum";

/* work count */
long count = 0;

void increment_workcount() {
	if(ENABLE_WORKCOUNT){
		#pragma omp critical
		{
		    count++;
		}
	}
}

void iterative_prefix_sum(generic_type* x, long n) {
	long kk,k,i;

    for (k=1; k<n;k=kk) {
        kk = k<<1; // double
        #pragma omp parallel for private(i) schedule(dynamic)
        for (i=kk-1; i<n; i+=kk) {
            x[i] = x[i-k]+x[i];					WORKCOUNTER();
        }
    }
    
    for (k=k<<1; k>1;k=kk) {
        kk = k>>1; // halve
        #pragma omp parallel for private(i) schedule(dynamic)
        for (i=k-1; i<n-kk; i+=k) {
            x[i+kk] = x[i]+x[i+kk];				WORKCOUNTER();
        }
    }

}

void init(int argc, char* argv[], long* n, generic_type** arr) {
	/* number of elements */
	*n = parse_args(argc, argv);

	/* 1 is the maximum number of shifts we might do to correct for the identity element */
	*arr = (generic_type*) malloc(sizeof(generic_type) * (*n + 1));

	if(*n <= 0) {
		(void) fprintf(stdout, "n = 0, nothing to do\n\n");
		bail_out(EXIT_SUCCESS, "n = 0, nothing to do");
	}

	/* seed the random number generator */
	DEBUG("seeding srand...\n");
	srand(time(NULL));

	/* init test data */
	fill_with_random_numbers(*arr, *n);
	DEBUG("printing the input array...\n");
	print_arr(*arr, *n);
 
    if(DO_CHECK) {
        write_arr_to_file(*arr, *n, FILE_INPUT_ARRAY);
    }
}

void finalize(long n, generic_type* arr) {
	if(ENABLE_WORKCOUNT)
		(void) fprintf(stdout, "The total number of operations is: %ld\n", count);

	/* output */
	DEBUG("printing the result array...\n");
	print_arr(arr, n);
    
    if(DO_CHECK) {
        write_arr_to_file(arr, n, FILE_RESULT_ARRAY);
    }
}

int main(int argc, char* argv[]) {

	long n;
	generic_type* arr;
	init(argc, argv, &n, &arr);

	// start measurement
	double t_start, t_end;
	t_start = omp_get_wtime();

    // Calculate
	iterative_prefix_sum(arr, n);

    // measurement
    t_end = omp_get_wtime();
	(void) fprintf(stdout, "Total openmp time: %.10f\n", t_end - t_start);

	finalize(n, arr);
  	return 0;
}
