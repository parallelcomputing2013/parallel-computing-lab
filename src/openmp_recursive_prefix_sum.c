/**
Recursive prefix sum algorithm with OpenMP
by Manuel Geier, Thomas Rieder

Usage:
./openmp_recursive_prefix_sum -n NUMBER_OF_ELEMENTS
**/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <errno.h>
#include <math.h>
#include <stdarg.h>
#include <time.h>
#include <limits.h>
#include <string.h>
#include "common.h"
#include <omp.h>

/*
 * sequential cutoff (level/current depth of the tree)
 */
#define SEQ_CUTOFF (15)

/* enables work count measurements */
#define ENABLE_WORKCOUNT (false)
//#define WORKCOUNTER() (increment_workcount()) //on
#define WORKCOUNTER() //off

/* Name of the program */
const char *progname = "openmp_recursive_prefix_sum";

/* work count */
long count = 0;

void increment_workcount() {
	#pragma omp critical
	{
	    count++;
	}
}


generic_type up_par(generic_type* arr, long d, long i, long n) {
	generic_type r_left, r_right;
    long j, k, inc_seq, seq_idx1, seq_idx2, end, inc;
	long recursion_level = log2(n) - d;

	if(d == 0) {
		return arr[i];
	} else if(recursion_level >= SEQ_CUTOFF) {
		DEBUG("doing sequential \"up\" processing\n");

		end = i + pow(2,d) - 1;

		DEBUG("d = %ld, start index = %ld, stop index = %ld\n", d, i , end);

		for(j=0; j < d; j++) {
			inc_seq = pow(2,j+1);
			for(k=i; k <= end; k += inc_seq) {
				seq_idx1 = k + pow(2,j+1) - 1;
				seq_idx2 = k + pow(2,j) - 1;
				arr[seq_idx1] = arr[seq_idx2] + arr[seq_idx1];

				if(ENABLE_WORKCOUNT)
					increment_workcount();
			}
		}

		return arr[end];

	} else {
		DEBUG("i am responsible for depth = %ld and indices from %ld to %ld\n", d, i, end);

		end = i + pow(2,d) - 1;
		inc = pow(2, d-1);

		#pragma omp parallel
		{
			#pragma omp sections
			{
				#pragma omp section
				r_left = up_par(arr, d-1, i, n);
				#pragma omp section
				r_right = up_par(arr, d-1, i + inc, n);
			}
		}
		arr[end] = r_left + r_right;					WORKCOUNTER();

		return arr[end];
	}
}


void down_par(generic_type* arr, long d, long offset, long n, generic_type prev_remainder) {
	generic_type temp, rem = 0;
    long i, j, inc2;

	long inc = pow(2, log2(n) - d - 1);
	long idx1 = inc - 1 + offset;
	long idx2 = 2 * inc - 1 + offset;

	DEBUG("switching the two indices: %ld <=> %ld\n", idx1, idx2);


	if(d == log2(n) - 1) {
		rem = prev_remainder;
	}

	temp = arr[idx1] + rem;
	arr[idx1] = arr[idx2] + rem;
	arr[idx2] = arr[idx2] + temp;						WORKCOUNTER();

	DEBUG("idx1 value: %d, idx2 value: %d\n", arr[idx1], arr[idx2]);
	DEBUG("PAR switched %d and %d\n", idx1, idx2);

	if(d == log2(n) - 1) {
		DEBUG("buttom level reached\n");
	} else if(d >= SEQ_CUTOFF) {
		DEBUG("starting sequential \"down\" processing\n");

		DEBUG("d = %d, offset = %d, inc = %d, idx1 = %d, idx2 = %d, end = %d\n", d, offset, inc, idx1, idx2, offset + 2*inc - 1);

		for(i = log2(n) - 1 - d - 1; i >= 0; i--) {
			DEBUG("doing level %d now\n",i);

			if(i == 0) {
				rem = prev_remainder;
			}

			inc2 = pow(2, i+1);
			for(j = offset; j <= offset + 2 * inc - 1; j +=  inc2) {
				idx1 = j + pow(2,i) - 1;
				idx2 = j + inc2 - 1;

				DEBUG("switching %d and %d\n", idx1, idx2);

				temp = arr[idx1] + rem;
				arr[idx1] = arr[idx2] + rem;
				arr[idx2] = arr[idx2] + temp;				WORKCOUNTER();
			}
		}
	} else {
		#pragma omp parallel
		{
			#pragma omp sections
			{
				#pragma omp section
				down_par(arr, d+1, offset, n, prev_remainder);
				#pragma omp section
				down_par(arr, d+1, offset + inc, n, prev_remainder);
			}
		}
	}
}


long prefix_sum(generic_type* arr, long n)
{
	generic_type last_element, down_remainder;
    long i, size, offset = 0;
	generic_type *arr2;

	// c bit hacking
	bool pow2 = (n & (n - 1)) == 0;

	if(pow2 == true) {
		last_element = arr[n-1];

		// up
		arr[n-1] = up_par(arr, log2(n), 0, n);
		print_arr(arr,n);

		DEBUG("---------------\n");

		// clear
		arr[n-1] = 0L;

		// down
		down_par(arr, 0, 0, n, 0);
		print_arr(arr, n);

		// fix first/last element
		arr[n] = arr[n-1] + last_element;			WORKCOUNTER();

		return 1;

	} else {
		DEBUG("n is not a power of two\n");

		// do all the ups
		for(i = 31; i >= 1; i--) {
			if(n & (1 << i)) {
				size = pow(2, i);

				DEBUG("bit %d is set, i would slice out %d\n", i, size);
				arr2 = arr + offset;

				arr2[size-1] = up_par(arr2, log2(size), 0, size);

				offset += size;
			}
		}

		offset = 0;
		down_remainder = 0;

		// do all the downs
		for(i = 31; i >= 0; i--) {
			if(n & (1 << i)) {

				size = pow(2, i);

				DEBUG("bit %d is set, i would slice out %d\n", i, size);

				arr2 = arr + offset;

				if(i == 0) {
					// there is only one element - no down
					arr2--;
					arr2[1] = arr2[1] + arr2[0];			WORKCOUNTER();
				} else {

					last_element = arr2[size-1];

					// set identity at the end of the current slice
					arr2[size-1] = 0L;

					DEBUG("using down_remainder: %d\n", down_remainder);
					down_par(arr2, 0, 0, size, down_remainder);

					// copy result to arr from arr2 - can probably be optimized
					memcpy(arr2, arr2+1, sizeof(generic_type)*(size-1));

					arr2[size-1] = last_element + down_remainder;
					down_remainder = arr2[size-1];				WORKCOUNTER();

					offset += size;
				}
			}
		}
		return 0;
	}
}



int main(int argc, char* argv[]) {

	generic_type ret = 0;

	/* number of elements */
	long n = parse_args(argc, argv);

	/* 1 is the maximum number of shifts we might do to correct for the identity element */
	generic_type* arr = (generic_type*) malloc(sizeof(generic_type) * (n + 1));

	if(n <= 0) {
		(void) fprintf(stdout, "n = 0, nothing to do\n\n");
		bail_out(EXIT_SUCCESS, "n = 0, nothing to do");
	}

	/* seed the random number generator */
	DEBUG("seeding srand...\n");
	srand(time(NULL));

	/* init test data */
	fill_with_random_numbers(arr, n);
	DEBUG("printing the input array...\n");
	print_arr(arr, n);

    if(DO_CHECK) {
        write_arr_to_file(arr, n, FILE_INPUT_ARRAY);
    }

	// start measurement
	double t_start, t_end;
	t_start = omp_get_wtime();

    // Calculate
	ret = prefix_sum(arr, n);

	if(ret == 1) {
		arr++;
	}

    // measurement
    t_end = omp_get_wtime();
	(void) fprintf(stdout, "Total openmp time: %.5f\n", t_end - t_start);

	/* output */
	DEBUG("printing the result array...\n");
	print_arr(arr, n);

    if(DO_CHECK) {
        write_arr_to_file(arr, n, FILE_RESULT_ARRAY);
    }

	if(ENABLE_WORKCOUNT)
		(void) fprintf(stdout, "The total number of operations is: %ld\n", count);

  	return 0;
}
