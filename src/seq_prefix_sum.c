/**
 * Sequential algorithm of prefix-sums
 * by Manuel Geier, Thomas Rieder
 *
 * it reads an array from a file and verifies it
 *
 * [1] = http://stackoverflow.com/questions/17502263/how-to-measure-cpu-time-and-wall-clock-time
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <errno.h>
#include <math.h>
#include <stdarg.h>
#include <time.h>
#include <limits.h>
#include <assert.h>
#include "common.h"

const char *progname = "seq_prefix_sum";

void seq_prefix_sum(generic_type* arr, long len);

int main (int argc, char* argv[])
{
	// number of elements
	long n = parse_args(argc, argv);

	if(n <= 0) {
		(void) fprintf(stdout, "CHECK SUCCESSFUL - Results match\n\n");
		bail_out(EXIT_SUCCESS, "n = 0, nothing to do");
	}

	// init
	generic_type* arr = (generic_type*) malloc(sizeof(generic_type) * n);
	read_arr_from_file(arr, n, FILE_INPUT_ARRAY);
	DEBUG("input array:\n");
	print_arr(arr, n);

	// start benchmark
	struct timespec now, tmstart;
	clock_gettime(CLOCK_REALTIME, &tmstart);

	// compute
	seq_prefix_sum(arr, n);

	// stop benchmark - taken from [1]
	clock_gettime(CLOCK_REALTIME, &now);
	(void) fprintf(stdout, "Total sequential time: %.5f\n",(double)((now.tv_sec+now.tv_nsec*1e-9) - (double)(tmstart.tv_sec+tmstart.tv_nsec*1e-9)));


	// output
	DEBUG("result array:\n");
	print_arr(arr, n);

	generic_type* comp = (generic_type*) malloc(sizeof(generic_type) * n);
	read_arr_from_file(comp, n, FILE_RESULT_ARRAY);
	DEBUG("comparison array:\n");
	print_arr(comp, n);

	long i;
	for(i = 0; i < n; i++) {
		if(arr[i] != comp[i]) {
			bail_out(EXIT_FAILURE, "CHECK FAILED - Results do not match\n");
		}
	}

	(void) fprintf(stdout, "CHECK SUCCESSFUL - Results match\n\n");

  	return 0;
}


void seq_prefix_sum(generic_type* arr, long len)
{
	if(len > 0)
	{
		generic_type sum = arr[0];
		for(long i=1; i<=len-1; i++)
		{
			sum += arr[i];
			arr[i] = sum;
		}
	}
}